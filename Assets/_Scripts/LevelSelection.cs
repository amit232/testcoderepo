﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using FlurrySDK;

public class LevelSelection : MonoBehaviour {

	public ObserverCollider[] allColliders;
	public GameObject[] levels;
	public GameObject[] levelPages;
	public GameObject goPages;
	public GameObject goFadePlane;
	public Transform trBtnNext, trBtnPrev;
	public Transform[] trLevelPaging;
	public Sprite[] Star;
	public static int selectedLevel = 0;
	 int nextPage=0,currPage=0;
	public  AudioSource auBgMisic,auClick;

	void Awake()
	{
		Time.timeScale = 1.0f;
		gameplay.isLeft = false;
		gameplay.isRight = false;
		gameplay.isLevelClear = false;
		gameplay.isStarGames = false;
		gameplay.isLevelFail = false;
		gameplay.isPaused = false;
		gameplay.isPopUp = false;
		gameplay._isGamePause = false;

		auBgMisic = GameObject.Find ("auBg").GetComponent<AudioSource>();
	}
	// Use this for initialization
	void Start () 
	{
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		foreach (ObserverCollider actCollider in allColliders) 
		{
			actCollider.TouchUp +=new ObserverColliderTouchEventHandler(ProcessButtonsUp);
			actCollider.TouchDown += new ObserverColliderTouchEventHandler (ProcessButtonsDown);
		}

		if(!PlayerPrefs.HasKey("currPage")) 
		{
			PlayerPrefs.SetInt("currPage",0);
		}
		currPage = PlayerPrefs.GetInt("currPage");
		iTween.RotateTo (levelPages[currPage], iTween.Hash ("z",0,"time", 0.2f, "easetype", iTween.EaseType.linear, "ignoretimescale", true));
		iTween.ScaleTo (levelPages[currPage], iTween.Hash ("x",1,"y",1,"time", 0.2f, "easetype", iTween.EaseType.linear, "ignoretimescale", true));
		trLevelPaging[trLevelPaging.Length-1].localPosition = new Vector3(trLevelPaging[currPage].localPosition.x,-0f,0f);

		setNextPrevBtns (currPage);

		if (!Menu.blnSound) 
		{
			auClick.mute = true;
		} 
		else 
		{
			auClick.mute = false;
		}

		if (!Menu.blnMusic) 
		{
			auBgMisic.mute = true;
		} 
		else 
		{
			auBgMisic.mute = false;

			if (!auBgMisic.isPlaying) 
			{
				auBgMisic.mute = false;
				auBgMisic.Play ();
			}
		}
		SetLevels ();
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
	public void ProcessButtonsDown(ObserverTouch source)
	{
		string buttonName = source.TouchCollider.name;
		Transform tr = source.TouchCollider.transform;

		auClick.Play ();

		if (source.TouchCollider.tag == "tagLevels") 
		{
			animPunchScale(tr.gameObject,"",0.3f);
		}
		else if (buttonName == "btnBack")
		{
			animPunchScale(tr.gameObject,"",0.3f);
		}
		else if (buttonName == "btnNext")
		{
			StartCoroutine ("AllCollider");
			animPunchScale(tr.gameObject,"",0.3f);
		}
		else if (buttonName == "btnPrev")
		{
			StartCoroutine ("AllCollider");
			animPunchScale(tr.gameObject,"",0.3f);
		}
	}
	public void ProcessButtonsUp(ObserverTouch source)
	{
		string buttonName = source.TouchCollider.name;

		if (source.TouchCollider.tag == "tagLevels") 
		{
				selectedLevel = int.Parse (buttonName);

				if (CommonFunctions.getGameDictionaryData ("level" + selectedLevel +"world"+Worlds_Selection.selecteWorlds+"locked") == "yes") 
				{		
				
				}
				else 
				{
                    // Log Flurry events.
                    Flurry.EventRecordStatus status = Flurry.LogEvent("Level No.:"+ Worlds_Selection.selecteWorlds + "_"+ selectedLevel);
                    gameplay.CntUnlockLevel = 0;
						FadeIn("ShowTutorials");
				}
		}
		else if (buttonName == "btnBack")
		{
			FadeIn("ShowWorlds");
		}
		else if (buttonName == "btnNext" && nextPage < CommonFunctions.totalLevelPages - 1) 
		{
			if (currPage < CommonFunctions.totalLevelPages - 1) 
			{
				nextPage = currPage;
				nextPage++;
			}
			iTween.ScaleTo(levelPages[currPage],iTween.Hash("x",0,"y",0,"time",0.2f,"islocal",true,"easetype",iTween.EaseType.easeInBack,"oncomplete","ShowNextPage",
															"oncompleteparams",nextPage,"oncompletetarget",gameObject,"ignoretimescale",true));
			iTween.RotateTo(levelPages[currPage],iTween.Hash("z",180,"time",0.2f,"easetype",iTween.EaseType.linear,"ignoretimescale",true));
		} 
		else if (buttonName == "btnPrev") 
		{
			if (currPage > 0) 
			{
				nextPage = currPage;
				nextPage--;
			}
			iTween.ScaleTo(levelPages[currPage],iTween.Hash("x",0,"y",0,"time",0.2f,"islocal",true,"easetype",iTween.EaseType.easeInBack,"oncomplete","ShowNextPage",
															"oncompleteparams",nextPage,"oncompletetarget",gameObject,"ignoretimescale",true));
			iTween.RotateTo(levelPages[currPage],iTween.Hash("z",180,"time",0.2f,"easetype",iTween.EaseType.linear,"ignoretimescale",true));
		} 
	}
		
	void ShowNextPage(int pageNo)
	{
		iTween.RotateTo(levelPages[pageNo],iTween.Hash("z",0,"time",0.2f,"easetype",iTween.EaseType.linear,"ignoretimescale",true));
		iTween.ScaleTo(levelPages[pageNo],iTween.Hash("x",1,"y",1,"time",0.2f,"easetype",iTween.EaseType.linear,"ignoretimescale",true));
		PlayerPrefs.SetInt("currPage",pageNo);
		currPage = PlayerPrefs.GetInt("currPage");
		trLevelPaging[trLevelPaging.Length-1].localPosition = new Vector3(trLevelPaging[currPage].localPosition.x,0f,0f);
		setNextPrevBtns (pageNo);	
	}
	void setNextPrevBtns(int pgNo) 
	{
		if (pgNo == 0) 
		{
			trBtnPrev.gameObject.SetActive (false);
			trBtnNext.gameObject.SetActive (true);
		} 
		else if (pgNo == CommonFunctions.totalLevelPages - 1) 
		{
			trBtnPrev.gameObject.SetActive (true);
			trBtnNext.gameObject.SetActive (false);
		}
		else 
		{
			trBtnPrev.gameObject.SetActive (true);
			trBtnNext.gameObject.SetActive (true);
		}
	}
	void ShowTutorials()
	{
		if (PlayerPrefs.GetInt("SetTutorials") == 0) 
		{
			FadeOut ();
			Menu.gameStatus = Menu.GameStatus.Tutorials;
			SceneManager.LoadScene ("gameplay");
		}
		else if (PlayerPrefs.GetInt("SetTutorials") == 1) 
		{
			FadeOut ();
			Menu.gameStatus = Menu.GameStatus.Gameplay;
			SceneManager.LoadScene ("gameplay");
		}
		Menu.selectedColors = PlayerPrefs.GetInt ("SelectedColors");
		Menu.selectedPlayer = PlayerPrefs.GetInt ("SelectedPlayers");

	}
	void ShowWorlds()
	{
		FadeOut ();
		Menu.gameStatus = Menu.GameStatus.Worlds;
		SceneManager.LoadScene ("gameplay");
	}

	int gainStars=0;

	public  void SetLevels()
	{
		for(int i=0; i< CommonFunctions.totalLevels; i++)
		{	
			gainStars = int.Parse( CommonFunctions.getGameDictionaryData ("level" +i.ToString()+"world"+Worlds_Selection.selecteWorlds.ToString()+"star"));

			if( CommonFunctions.getGameDictionaryData ("level"+i.ToString()+"world"+Worlds_Selection.selecteWorlds.ToString()+"locked") == "yes")
			{
				levels[i].transform.Find("lock").gameObject.SetActive(true);
				levels[i].transform.Find("unlock").gameObject.SetActive(false);
			}
			else  
			{
				levels[i].transform.Find("lock").gameObject.SetActive(false);
				levels[i].transform.Find("unlock").gameObject.SetActive(true);

				for(int n=0;n < 3; n++) 
				{
					if (n < gainStars)
						levels [i].transform.Find ("star" + n).GetComponent<SpriteRenderer> ().sprite = Star [1];
					else 
						levels[i].transform.Find("star"+n).GetComponent<SpriteRenderer>().sprite = Star[0];
				}
			}
		}

	}
		
	void animPunchScale(GameObject go,string fun,float scalevalue)
	{
		iTween.PunchScale(go,iTween.Hash("x",scalevalue,"y",scalevalue,"easetype",iTween.EaseType.easeOutBack,"time",0.5f,"ignoretimescale",true,"oncomplete",fun,"oncompletetarget",gameObject));
	}

	float fadeInOutTime=1.0f;

	public void FadeOut()
	{
		iTween.FadeTo(goFadePlane,iTween.Hash("amount",0.0,"time",fadeInOutTime,"easetype",iTween.EaseType.linear,"ignoretimescale",true));
	}

	public void FadeIn(string method)
	{
		CommonFunctions.EnableColliders (false,allColliders);
		iTween.FadeTo(goFadePlane,iTween.Hash("amount",1.0,"time",fadeInOutTime,"easetype",iTween.EaseType.linear,"oncomplete",method,"oncompletetarget",gameObject,"ignoretimescale",true));
	}

	IEnumerator AllCollider()
	{
		CommonFunctions.EnableColliders (false,allColliders);
		yield return new WaitForSeconds (0.6f);
		CommonFunctions.EnableColliders (true,allColliders);
	}
}
