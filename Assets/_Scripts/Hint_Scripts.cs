﻿using UnityEngine;
using System.Collections;

public class Hint_Scripts : MonoBehaviour {

	public float scalevalue;
	public float delaytime=2;

	public GameObject particle_Object;

	// Use this for initialization
	void Start () 
	{
		iTween.ScaleTo(this.gameObject,iTween.Hash("x",scalevalue,"y",scalevalue,"z",scalevalue,"delay",delaytime,"easetype",iTween.EaseType.linear,"looptype",iTween.LoopType.pingPong));

			InvokeRepeating("colliderscale",0.0f,0.1f);
	}
		
	void colliderscale()
	{
		if (this.transform.localScale.x == scalevalue || this.transform.localScale.y == scalevalue) 
		{
			particle_Object.SetActive (true);
		} 
		else 
		{
			particle_Object.SetActive (false);
		}
	}
}
