﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using FlurrySDK;

public class Worlds_Selection : MonoBehaviour 
{
	public ObserverCollider[] allColliders;
	public GameObject[] Worlds;
	public GameObject goFadePlane;
	public static int selecteWorlds = 0;
	public  AudioSource auBgMisic,auClick;
	float fadeInOutTime=1.0f;

	void Awake()
	{
		Menu.selectedPlayer = PlayerPrefs.GetInt ("SelectedPlayers");
		Time.timeScale = 1.0f;
		gameplay.isLeft = false;
		gameplay.isRight = false;
		gameplay.isLevelClear = false;
		gameplay.isStarGames = false;
		gameplay.isLevelFail = false;
		gameplay.isPaused = false;
		gameplay.isPopUp = false;
		gameplay._isGamePause = false;
		auBgMisic = GameObject.Find ("auBg").GetComponent<AudioSource>();

		Debug.Log ("TagColorsBTN->>>>>CurrentPlayer->>>>>>>>>"    +  Menu.currPlayers    +   "   SelectedPlayer->>>>>>>>  "  +  Menu. selectedPlayer   + "   CurrentColors->>>>>>>>>>>>>>>    "  +   Menu.selectedColors );
	}

	// Use this for initialization
	void Start () 
	{
		Screen.sleepTimeout = SleepTimeout.NeverSleep;

		foreach (ObserverCollider actCollider in allColliders) 
		{
			actCollider.TouchUp +=new ObserverColliderTouchEventHandler(ProcessButtonsUp);
			actCollider.TouchDown += new ObserverColliderTouchEventHandler (ProcessButtonsDown);
		}

		if (!Menu.blnSound) 
		{
			auClick.mute = true;
		} 
		else 
		{
			auClick.mute = false;
		}

		if (!Menu.blnMusic) 
		{
			auBgMisic.mute = true;
		} 
		else 
		{
			auBgMisic.mute = false;

			if (!auBgMisic.isPlaying) 
			{
				auBgMisic.mute = false;
				auBgMisic.Play ();
			}
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void ProcessButtonsDown(ObserverTouch source)
	{
		string buttonName = source.TouchCollider.name;
		Transform tr = source.TouchCollider.transform;

		auClick.Play ();

		if (source.TouchCollider.tag == "tagWorlds") 
		{
			animPunchScale(tr.gameObject,"",0.3f);
		}
		else if (buttonName == "btnBack")
		{
			StartCoroutine ("AllCollider");
			animPunchScale(tr.gameObject,"",0.3f);
		}
	}
	public void ProcessButtonsUp(ObserverTouch source)
	{
		string buttonName = source.TouchCollider.name;

		if (source.TouchCollider.tag == "tagWorlds") 
		{
			selecteWorlds = int.Parse (buttonName);
            // Log Flurry events.
            Flurry.EventRecordStatus status = Flurry.LogEvent("World: "+buttonName);
            FadeIn ("ShowLevels");
		}
		else if (buttonName == "btnBack")
		{
			FadeIn ("ShowMenu");
		}
	}


	void ShowMenu()
	{
		FadeOut ();
		Menu.gameStatus = Menu.GameStatus.Menu;
		SceneManager.LoadScene ("gameplay");
	}
	void ShowLevels()
	{
		FadeOut ();
		Menu.gameStatus = Menu.GameStatus.Levels;
		SceneManager.LoadScene ("gameplay");
	}



	void animPunchScale(GameObject go,string fun,float scalevalue)
	{
		iTween.PunchScale(go,iTween.Hash("x",scalevalue,"y",scalevalue,"easetype",iTween.EaseType.easeOutBack,"time",0.5f,"ignoretimescale",true,"oncomplete",fun,"oncompletetarget",gameObject));
	}
	public void FadeOut()
	{
		iTween.FadeTo(goFadePlane,iTween.Hash("amount",0.0,"time",fadeInOutTime,"easetype",iTween.EaseType.linear,"ignoretimescale",true));
	}

	public void FadeIn(string method)
	{
		CommonFunctions.EnableColliders (false,allColliders);
		iTween.FadeTo(goFadePlane,iTween.Hash("amount",1.0,"time",fadeInOutTime,"easetype",iTween.EaseType.linear,"oncomplete",method,"oncompletetarget",gameObject,"ignoretimescale",true));
	}

	IEnumerator AllCollider()
	{
		CommonFunctions.EnableColliders (false,allColliders);
		yield return new WaitForSeconds (1.5f);
	}
}
