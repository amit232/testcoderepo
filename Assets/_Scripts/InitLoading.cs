﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InitLoading : MonoBehaviour
{
    public OneAudienceUnity objOneAudience;
    void Awake()
    {
        if(Application.platform == RuntimePlatform.IPhonePlayer)
            objOneAudience.init("1189C876-722F-45DD-9AD7-3CF15D8F52D1");
    }
    // Start is called before the first frame update
    void Start()
    {
        Social.localUser.Authenticate(success =>
        {
            if (success)
            {
                Debug.Log("Authentication successful");
                string userInfo = "Username: " + Social.localUser.userName +
                    "\nUser ID: " + Social.localUser.id +
                    "\nIsUnderage: " + Social.localUser.underage;
                Debug.Log(userInfo);
            }
            else
                Debug.Log("Authentication failed");
        });

        objOneAudience.setEmailAddress("Device-Email");

        Invoke("LoadGame", 3.0f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void LoadGame()
    {
        SceneManager.LoadScene("gameplay");
    }
}
