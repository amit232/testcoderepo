﻿using UnityEngine;
using System.Collections;

public class Level_Rotation : MonoBehaviour 
{
	public gameplay ObjGamePlay;
	bool isLeft,isRight,isActive=false;
	// Use this for initialization
	void Start () 
	{
		//ObjGamePlay =GameObject.Find("Script_Object").GetComponent<gameplay>();

	}
	
	// Update is called once per frame
	void Update () 
	{
		if (gameplay.isLevelFail || gameplay.isLevelClear || gameplay.isPaused)
							return;


		 if (Input.GetKeyUp (KeyCode.LeftArrow) || Input.GetKeyUp (KeyCode.RightArrow)) 
		{
			gameplay.isLeft = false;
			gameplay.isRight = false;
		} 
		else if (Input.GetKeyDown (KeyCode.RightArrow)) 
		{
			gameplay.isRight = true;
			gameplay.isLeft = false;

			if (!isActive) 
			{
                Debug.Log(ObjGamePlay.Players[Menu.selectedPlayer].name);
				isActive = true;
//				ObjGamePlay.Player.isKinematic = false;
				ObjGamePlay.Players [Menu.selectedPlayer].GetComponent<Rigidbody2D> ().isKinematic = false;
//				PlayerRigid.isKinematic = false;
			}

		} 
		else if (Input.GetKeyDown (KeyCode.LeftArrow)) 
		{
			gameplay.isLeft = true;
			gameplay.isRight = false;

			if (!isActive) 
			{
				isActive = true;
				ObjGamePlay.Players [Menu.selectedPlayer].GetComponent<Rigidbody2D> ().isKinematic = false;
//				ObjGamePlay.Player.isKinematic = false;
//				PlayerRigid.isKinematic = false;
			}

		}

		///// -------------------------------------- /////
			
		if (gameplay.isLeft && !gameplay.isRight)	 
		{
			transform.Rotate (Vector3.forward * 40 * Time.deltaTime);
			gameplay.isRight=false;
		}
		else if (gameplay.isRight && !gameplay.isLeft) 
		{
			transform.Rotate (Vector3.back * 40 * Time.deltaTime);
			gameplay.isLeft = false;
		}
		else
		{
			transform.Rotate (Vector3.zero * 0 * Time.deltaTime);
			gameplay.isLeft = false;
			gameplay.isRight = false;
		}


	}
}
