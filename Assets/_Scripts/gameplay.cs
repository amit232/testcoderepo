﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.Advertisements;

public class gameplay : MonoBehaviour 
{
	public GameObject[] Levels,Players;
	public ObserverCollider[] allColliders;
	public ObserverCollider[] PopUpColliders;
	public GameObject PopUpPaused,PopUpLevelCleared,PopUpEndGames,PopUpLevelUnlock,PopUpInternet_Connection,PopUpVideoAds,goFadePlane,PopUpBg,Time_Object;
	public static bool isRight,isLeft,isLevelFail, isLevelClear,isStarGames,isPaused,_isGamePause,isPopUp,isTutorials;
	public static GameObject goTemp,goKeyLock_Temp;
	public AudioSource auClick,auBgMisic,auCoin,auIn_Swap,auOut_Swap,auWin,auKey,auHit,auStar;
	GameObject goKey,goKeyLock;
	GameObject[] gos = new GameObject[3];
	public int cntStars=0,cnt,score = 0;
	public static int cntDiamond=0;
	int addStars,isShowTutorials=0;
	public Sprite[] SrtSfx;
	public TextMesh tmScore,dispLevels,dispTime;
	public Color[] Player_Color;

	//Time-Count//
	private const string strQualifyTIME = "{0:D2}:{1:D2}.{2:D3}";
	public System.TimeSpan tsTimeLeft;
	bool isTimeFinished;
	public System.Diagnostics.Stopwatch stopWatch = new System.Diagnostics.Stopwatch();
	private const string strTIME = "{0:D2}:{1:D2}";
	double 	totalTime = 0d;
	public double targetTime=0d;


	void Awake()
	{

		Time.timeScale = 1.0f;
		isPaused = false;
		_isGamePause = false;
		isRight = false;
		isLeft = false;
		isLevelClear = false;
		isLevelFail = false;
		isStarGames = false;
		isPopUp = false;

		Levels [LevelSelection.selectedLevel].SetActive (true);

	
	
		Menu.selectedPlayer=PlayerPrefs.GetInt ("SelectedPlayers");

		Players [Menu.selectedPlayer].SetActive (true);
		 
		Players [Menu.selectedPlayer].GetComponent<SpriteRenderer> ().color = Player_Color [Menu.selectedColors];

		auBgMisic = GameObject.Find ("auBg").GetComponent<AudioSource>();

	
		dispLevels.text = (LevelSelection.selectedLevel + 1).ToString ("00");

		Debug.Log ("SkipLevels-->>    "  + PlayerPrefs.GetInt ("UnlockLevelCounter") );
	}

	// Use this for initialization
	void Start () 
	{

		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		
		Players[Menu.selectedPlayer].transform.position =GameObject.Find ("Levels/level"+(LevelSelection.selectedLevel + 1).ToString()+"/Roter_Levels/StartPoints").transform.position;

		foreach (ObserverCollider actCollider in allColliders)
		{
			actCollider.TouchDown += new ObserverColliderTouchEventHandler (processButtonsDown);
			actCollider.TouchUp += new ObserverColliderTouchEventHandler (processButtonsUp);
		}
		foreach (ObserverCollider actCollider in PopUpColliders)
		{
			actCollider.TouchDown += new ObserverColliderTouchEventHandler (PopupButtonsDown);
			actCollider.TouchUp += new ObserverColliderTouchEventHandler (PopupButtonsUp);
		}
		goKey = GameObject.Find ("Key");
		goKeyLock = GameObject.Find ("KeyLock");

		if (goKey == null) 
		{
			goTemp = GameObject.Find ("EndPoints");
			goTemp.GetComponent<RotateObject> ().enabled = true;
			goKeyLock_Temp = goKeyLock;
			iTween.ScaleTo (goKeyLock_Temp,iTween.Hash("x",0f,"y",0f,"time",0.01f,"easetype", iTween.EaseType.linear,"ignoretimescale",true));
			goKeyLock_Temp.SetActive (false);
		} 
		else 
		{
			goTemp = GameObject.Find ("EndPoints");
			goKeyLock_Temp = goKeyLock;
			iTween.ScaleTo (goKeyLock_Temp,iTween.Hash("x",3.2f,"y",3.2f,"time",0.01f,"easetype", iTween.EaseType.linear,"ignoretimescale",true));
			goTemp.GetComponent<RotateObject> ().enabled = false;
			goTemp.GetComponent<CircleCollider2D> ().isTrigger = true;
		}

		CheckMusic ();
		CheckSound ();

		if(Menu.blnMusic) 
		{
			if (!auBgMisic.isPlaying) 
			{
				auBgMisic.mute = false;
				auBgMisic.Play ();
			}
		}
			
		if (Worlds_Selection.selecteWorlds == 0) 
		{

		}
		else 
		{
			Players[Menu.selectedPlayer].GetComponent<Rigidbody2D>().gravityScale=0.3f;

			string strTargetTime = CommonFunctions.getGameDictionaryData ("level" + LevelSelection.selectedLevel.ToString () + "time");
			targetTime = strTargetTime.Length == 0 ? 0.0 : double.Parse(strTargetTime);
			targetTime += 0.0f;
			targetTime *= 1000;

			Time_Object.SetActive (true);
		}

		ShowTutorials ();

		if (Menu.gameStatus == Menu.GameStatus.Gameplay && CommonFunctions.CheckInternetConnection ()) 
		{
			CommonFunctions.LoadFullScreenAds ();
		}
	}
	
	// Update is called once per frame
	void Update () 
	{

		if (isPaused||isLevelClear||isLevelFail)
					return;


		if(Input.GetKeyDown(KeyCode.Escape))
		{
			OnApplicationPause(true);
			OnApplicationFocus(true);
		}



			Timer ();
	}
		
	public void ShowTutorials()
	{
		if (PlayerPrefs.GetInt("SetTutorials") == 0) 
		{
		}
		else if (PlayerPrefs.GetInt("SetTutorials") == 1) 
		{
		}
	}
	public void Timer()
	{ 
		if (Worlds_Selection.selecteWorlds == 1) 
		{
			stopWatch.Start ();

			totalTime = stopWatch.Elapsed.TotalMilliseconds;
			tsTimeLeft = System.TimeSpan.FromMilliseconds ((targetTime) - stopWatch.Elapsed.TotalMilliseconds);

			if (tsTimeLeft.Seconds >= 0 && tsTimeLeft.Milliseconds > 0.0f) 
			{
				dispTime.text = System.String.Format (strTIME, tsTimeLeft.Minutes, tsTimeLeft.Seconds, tsTimeLeft.Milliseconds.ToString ());
			}

			if (targetTime < totalTime && !isTimeFinished) 
			{
				if (isLevelFail && !isLevelClear)
							return;

				isTimeFinished = true;
				ShowLevelFailed ();
			}
		}
	}
	public void processButtonsDown(ObserverTouch source) 
	{
		string buttonName = source.TouchCollider.name;
		Transform tr = source.TouchCollider.transform;


		if (buttonName == "btnPause") 
		{	
			auClick.Play ();
			animPunchScale(tr.gameObject,"",0.3f);
		}
		else if (buttonName == "btnRight") 
		{	
			isRight = true;
			isLeft = false;
			Players [Menu.selectedPlayer].GetComponent<Rigidbody2D> ().isKinematic = false;
		}
		else if (buttonName == "btnLeft") 
		{	
			isRight = false;
			isLeft = true;
			Players [Menu.selectedPlayer].GetComponent<Rigidbody2D> ().isKinematic = false;
		}

	}
	public void processButtonsUp(ObserverTouch source)
	{
		string buttonName = source.TouchCollider.name;

		if (buttonName == "btnPause") 
		{	
			ShowPaused ();
		}
		else if (buttonName == "btnRight") 
		{	
			isRight = false;
		}
		else if (buttonName == "btnLeft") 
		{	
			isLeft = false;
		}
	}

	public void PopupButtonsDown(ObserverTouch source) 
	{
		string buttonName = source.TouchCollider.name;
		Transform tr = source.TouchCollider.transform;

		auClick.Play ();
		animPunchScale(tr.gameObject,"",0.3f);

		if (buttonName == "btnLCNext") 
		{
			CommonFunctions.EnablePopUpColliders (false,PopUpColliders);
		}
		else if (buttonName == "btnLCRestart") 
		{
			CommonFunctions.EnablePopUpColliders (false,PopUpColliders);
			HidePopUp (PopUpLevelCleared);
		}
		else if (buttonName == "btnLCLevels") 
		{
			CommonFunctions.EnablePopUpColliders (false,PopUpColliders);
			HidePopUp (PopUpLevelCleared);
		}
		else if (buttonName == "btnContinues") 
		{

		}
		else if (buttonName == "btnLevels") 
		{
			HidePopUp (PopUpPaused);

		}
		else if (buttonName == "btnMusic_On") 
		{
			
		}
		else if (buttonName == "btnRestart") 
		{
			
			HidePopUp (PopUpPaused);
		}
		else if (buttonName == "btnSound_On") 
		{
			
		}
		else if (buttonName == "btnGEHome") 
		{
			CommonFunctions.EnablePopUpColliders (false,PopUpColliders);
			HidePopUp (PopUpEndGames);

		}
		else if (buttonName == "btnGELevels") 
		{
			CommonFunctions.EnablePopUpColliders (false,PopUpColliders);
			HidePopUp (PopUpEndGames);

		}
		else if (buttonName == "btnGEResart") 
		{
			CommonFunctions.EnablePopUpColliders (false,PopUpColliders);
			HidePopUp (PopUpEndGames);

		}
		else if (buttonName == "btnUnlockYes") 
		{
			
		}
		else if (buttonName == "btnUnlockNo") 
		{

		}
		else if (buttonName == "btnINCNO") 
		{
			
		}
		else if (buttonName == "btnAdsNo") 
		{
				
		}
	}
	public void PopupButtonsUp(ObserverTouch source)
	{
		string buttonName = source.TouchCollider.name;

		if (buttonName == "btnLCNext") 
		{
			CntUnlockLevel = 0;
			if (LevelSelection.selectedLevel < CommonFunctions.totalLevels - 1) 
			{
				FadeIn ("ShowNextLevels");
			} 
			else if (LevelSelection.selectedLevel == CommonFunctions.totalLevels - 1) 
			{
				ShowEndGames ();
			}
		} else if (buttonName == "btnLCRestart") {
			FadeIn ("ShowRestart");
		} else if (buttonName == "btnLCLevels") {
			FadeIn ("ShowLevels");
		} else if (buttonName == "btnContinues") {
			HidePaused ();
		} else if (buttonName == "btnLevels") {
			FadeIn ("ShowLevels");
		} else if (buttonName == "btnSound_On") {
			if (Menu.blnSound) {
				Menu.blnSound = false;
				PopUpPaused.transform.Find ("btnSound_On").GetComponent<SpriteRenderer> ().sprite = SrtSfx [1];
				PlayerPrefs.SetInt ("GameSound", 0);
				AllSoundOff ();
			} else {
				Menu.blnSound = true;
				PopUpPaused.transform.Find ("btnSound_On").GetComponent<SpriteRenderer> ().sprite = SrtSfx [0];
				PlayerPrefs.SetInt ("GameSound", 1);
				AllSoundOn ();
			}
		} else if (buttonName == "btnMusic_On") {
			if (Menu.blnMusic) {
				Menu.blnMusic = false;
				auBgMisic.mute = true;
				PopUpPaused.transform.Find ("btnMusic_On").GetComponent<SpriteRenderer> ().sprite = SrtSfx [3];
				PlayerPrefs.SetInt ("GameMusic", 0);
			} else {
				Menu.blnMusic = true;
				auBgMisic.mute = false;
				PopUpPaused.transform.Find ("btnMusic_On").GetComponent<SpriteRenderer> ().sprite = SrtSfx [2];
				PlayerPrefs.SetInt ("GameMusic", 1);

				if (auBgMisic.isPlaying)
					auBgMisic.mute = true;
			}
		} 
		else if (buttonName == "btnRestart") 
		{
			FadeIn ("ShowRestart");
		} 
		else if (buttonName == "btnGEHome") 
		{
			FadeIn ("ShowMenu");
		} 
		else if (buttonName == "btnGELevels") 
		{
			FadeIn ("ShowLevels");
		} 
		else if (buttonName == "btnGEResart") 
		{
			FadeIn ("ShowRestart");
		} 
		else if (buttonName == "btnUnlockYes") 
		{
			if (CommonFunctions.CheckInternetConnection ()) 
			{
				if (Advertisement.IsReady ()) 
				{
					var options = new ShowOptions { resultCallback = HandleShowResult };
					Advertisement.Show (null, options);
				} 
				else 
				{
					HidePopUp (PopUpLevelUnlock);
					ShowPopUp (PopUpVideoAds);
				}
			} 
			else 
			{
				HidePopUp (PopUpLevelUnlock);
				ShowPopUp (PopUpInternet_Connection);
			}
		}
		else if (buttonName == "btnUnlockNo") 
		{
			CommonFunctions.EnableColliders (true,allColliders);
			OverFadeIn ("Show_Failed");	
		}
		else if (buttonName == "btnINCNO") 
		{
			CommonFunctions.EnableColliders (true,allColliders);
			OverFadeIn ("Show_Failed");	
		}
		else if (buttonName == "btnAdsNo") 
		{
			CommonFunctions.EnableColliders (true,allColliders);
			OverFadeIn ("Show_Failed");	
		}

	}

	void Allcollideringameplay()
	{
		CommonFunctions.EnableColliders (true,allColliders);
	}
	void ShowPaused()
	{
		Time.timeScale = 0.0f;
		isPaused = true;
		ShowPopUp (PopUpPaused);
		PopUpBg.SetActive (true);
		AllSoundOff ();
		stopWatch.Stop ();
		if (Menu.blnMusic)
			auBgMisic.mute = true;
		CommonFunctions.EnableColliders (false,allColliders);
		CommonFunctions.EnablePopUpColliders (true,PopUpColliders);

	}
	IEnumerator Popupbg_Ien()
	{
		yield return new WaitForSeconds (0.3f);
		PopUpBg.SetActive (false);
	}
	void HidePaused()
	{
		Time.timeScale = 1.0f;
		isPaused = false;
		HidePopUp (PopUpPaused);
		StartCoroutine ("Popupbg_Ien");
		AllSoundOn ();
		stopWatch.Start ();
		if (Menu.blnMusic)
			auBgMisic.mute = false;
		CommonFunctions.EnablePopUpColliders (false,PopUpColliders);
	}
	public void  ShowLevelCleared()
	{
		if (isLevelFail)
			return;
		int gpsScore = 0;
		isLevelClear = true;
		auWin.Play ();
		StartCoroutine ("LevelClear");
		CommonFunctions.EnableColliders (false,allColliders);
		CommonFunctions.EnablePopUpColliders (true,PopUpColliders);
		if (cntStars == 0)
		{
			score = 0;
		}
		else if (cntStars == 1)
		{
			score = 5;
			gpsScore = score * (score*2);
		}
		else if (cntStars == 2)
		{
			score = 10;
			gpsScore = score * (score*3);
		}
		else if (cntStars == 3)
		{
			score = 20;
			gpsScore = score * (score*4);
		}

		if(cntDiamond == 0)
		{
			cntDiamond = 0;
		}
		else if (cntDiamond == 1) 
		{
			cntDiamond = 50;
		}
			
		PopUpLevelCleared.transform.Find ("tmText/tmScore").GetComponent<TextMesh> ().text = (cntDiamond+score).ToString();
		PlayerPrefs.SetInt ("totalcoin",PlayerPrefs.GetInt("totalcoin")+(cntDiamond+score));

		int savedStars = int.Parse(CommonFunctions.getGameDictionaryData("level" +LevelSelection.selectedLevel+"world"+Worlds_Selection.selecteWorlds+"star"));

		if (savedStars < cntStars) 
		{
			CommonFunctions.changeGameDictionary ("level" + LevelSelection.selectedLevel +"world"+Worlds_Selection.selecteWorlds+"star", cntStars.ToString ());
			addStars = cntStars - savedStars;
		}

		PlayerPrefs.SetInt ("totalgamescore",PlayerPrefs.GetInt("totalgamescore")+gpsScore);
			if(Application.platform == RuntimePlatform.IPhonePlayer)
		{
			Social.ReportScore (PlayerPrefs.GetInt("totalgamescore"), CommonFunctions.HighScoresId, success => 
				{
				Debug.Log (success ? "Reported score successfully" : "Failed to report score");
			});
		}
	}

	IEnumerator LevelClear()
	{
		yield return new WaitForSeconds (0.5f);

		if (CommonFunctions.CheckInternetConnection ()) 
		{
            //if (Advertisement.IsReady ()) 
            //{
            //	Advertisement.Show ();
            //} 
            //else 
            //{

            //}
            CommonFunctions.ShowFullScreenAds();
		} 
		else 
		{

		}
		if (LevelSelection.selectedLevel == CommonFunctions.totalLevels - 1) 
		{
			for(int i=0;i<cntStars;i++) 
			{
				gos [i] = PopUpLevelCleared.transform.Find ("stary" + i).gameObject;
			}

			StartCoroutine("starAnim");
			StartCoroutine ("ShowLevelComplete");
		} 
		else 
		{
			for(int i=0;i<cntStars;i++) 
			{
				gos [i] = PopUpLevelCleared.transform.Find ("stary" + i).gameObject;
			}

			if (LevelSelection.selectedLevel < CommonFunctions.totalLevels - 1)
				CommonFunctions.changeGameDictionary ("level" +(LevelSelection.selectedLevel + 1)+"world"+(Worlds_Selection.selecteWorlds)+"locked", "no");

			StartCoroutine("starAnim");
			StartCoroutine ("ShowLevelComplete");
		}
	}
	IEnumerator starAnim()
	{
		yield return new WaitForSeconds (0.5f);
		if (cnt < cntStars) 
		{
			iTween.ScaleTo (gos[cnt], iTween.Hash ("x",3.5f, "y", 3.5f, "easetype", iTween.EaseType.linear, "time", 0.1,"islocal", true,"ignoretimescale",true));
			auStar.Play ();
			yield return new WaitForSeconds (0.3f);
			cnt++;
		}
		if (cnt < cntStars)
		{
			iTween.ScaleTo (gos[cnt], iTween.Hash ("x", 4.5f, "y", 4.5f, "easetype", iTween.EaseType.linear, "time", 0.1,"islocal", true,"ignoretimescale",true));
			auStar.Play ();
			yield return new WaitForSeconds (0.3f);
			cnt++;
		}	

		if (cnt < cntStars)
		{
			iTween.ScaleTo (gos[cnt], iTween.Hash ("x", 3.5f, "y", 3.5f, "easetype", iTween.EaseType.linear, "time", 0.1,"islocal", true,"ignoretimescale",true));
			auStar.Play ();
			yield return new WaitForSeconds (0.3f);
			cnt++;
		}	
	}

	IEnumerator ShowLevelComplete()
	{
		yield return new WaitForSeconds (0.4f);

		PopUpBg.SetActive (true);
		iTween.RotateTo (PopUpLevelCleared, iTween.Hash ("z",0,"time", 0.2f, "easetype", iTween.EaseType.linear, "ignoretimescale", true));
		iTween.ScaleTo (PopUpLevelCleared, iTween.Hash ("x",1,"y",1,"time", 0.2f, "easetype", iTween.EaseType.linear, "ignoretimescale", true));

	}

	public static int CntGameOver=0,CntUnlockLevel=0;
	int levelstatus=0;

	string levelst = "";

	public void ShowLevelFailed()
	{
		if (isLevelClear)
			 return;
	
		isLevelFail = true;
		CntGameOver++;
		CntUnlockLevel++;

		PlayerPrefs.SetInt ("GameOverCounter", CntGameOver);
		PlayerPrefs.SetInt ("UnlockLevelCounter", CntUnlockLevel);

		if (LevelSelection.selectedLevel < CommonFunctions.totalLevels - 1) 
		{
			levelst = CommonFunctions.getGameDictionaryData ("level" + (LevelSelection.selectedLevel + 1) + "world" + Worlds_Selection.selecteWorlds.ToString () + "locked");
		} 
		else if(LevelSelection.selectedLevel == CommonFunctions.totalLevels - 1)
		{
			levelst =CommonFunctions.getGameDictionaryData ("level" + (LevelSelection.selectedLevel) + "world" + Worlds_Selection.selecteWorlds.ToString () + "locked");
		}

		Debug.Log (PlayerPrefs.GetInt ("SaveLevels"));
	
		if (PlayerPrefs.GetInt ("GameOverCounter") >= 3) 
		{	
			CntGameOver = 0;

			if (CommonFunctions.CheckInternetConnection ()) 
			{
				CommonFunctions.ShowFullScreenAds ();
				OverFadeIn ("Show_Failed");	
			} 
			else 
			{
				OverFadeIn ("Show_Failed");	
			}
		} 
		else if (levelst == "yes")
		{
			if (PlayerPrefs.GetInt ("UnlockLevelCounter") >= 8) 
			{	
				if (LevelSelection.selectedLevel < CommonFunctions.totalLevels - 1) 
				{
					PlayerPrefs.SetInt ("UnlockLevelCounter", 0);
					CntUnlockLevel = 0;
					PopUpBg.SetActive (true);
					ShowPopUp (PopUpLevelUnlock);
					CommonFunctions.EnableColliders (false, allColliders);
				} 
				else if (LevelSelection.selectedLevel == CommonFunctions.totalLevels - 1) 
				{
					PlayerPrefs.SetInt ("UnlockLevelCounter", 0);
					CntUnlockLevel = 0;
					OverFadeIn ("Show_Failed");	
				}
			
			} 
			else 
			{
				OverFadeIn ("Show_Failed");	
			}
		} 
		else if (PlayerPrefs.GetInt ("UnlockLevelCounter") == 8 && PlayerPrefs.GetInt ("GameOverCounter") == 3) 
		{
			OverFadeIn ("Show_Failed");	
		} 
		else 
		{
			if(levelst == "yes")
			{
				OverFadeIn ("Show_Failed");	
			}
			else
			{
				PlayerPrefs.SetInt ("UnlockLevelCounter", 0);
				CntUnlockLevel=0;
				OverFadeIn ("Show_Failed");	
			}
		}
			
		isLeft = isRight = false;
		CommonFunctions.EnablePopUpColliders (true,PopUpColliders);
	}

	void Show_Failed()
	{
		OverFadeOut ();
		SceneManager.LoadScene ("gameplay");	
		CommonFunctions.EnablePopUpColliders (false,PopUpColliders);
	}

	void SkipLevels()
	{
		FadeOut ();

			CommonFunctions.changeGameDictionary ("level" + (LevelSelection.selectedLevel + 1)+ "world" + Worlds_Selection.selecteWorlds + "locked", "no");
			LevelSelection.selectedLevel++;

		SceneManager.LoadScene ("gameplay");
		CommonFunctions.EnablePopUpColliders (false,allColliders);
	}

	void ShowNextLevels()
	{
		FadeOut ();
		LevelSelection.selectedLevel++;
		SceneManager.LoadScene ("gameplay");
		CommonFunctions.EnablePopUpColliders (false,PopUpColliders);
	}
	void ShowEndGames()
	{
		PopUpBg.SetActive(true);
		ShowPopUp (PopUpEndGames);
		CommonFunctions.EnablePopUpColliders (true,PopUpColliders);
		CommonFunctions.EnableColliders (false,allColliders);
		iTween.RotateTo (PopUpLevelCleared, iTween.Hash ("z",180,"time", 0.2f, "easetype", iTween.EaseType.linear, "ignoretimescale", true));
		iTween.ScaleTo (PopUpLevelCleared, iTween.Hash ("x",0,"y",0,"time", 0.2f, "easetype", iTween.EaseType.linear, "ignoretimescale", true));
	}
	void ShowMenu()
	{
		FadeOut ();
		Menu.gameStatus = Menu.GameStatus.Menu;
		SceneManager.LoadScene ("gameplay");
		CommonFunctions.EnablePopUpColliders (false,PopUpColliders);
	}
	void ShowLevels()
	{
		FadeOut ();
		Menu.gameStatus = Menu.GameStatus.Levels;
		SceneManager.LoadScene ("gameplay");
		CommonFunctions.EnablePopUpColliders (false,PopUpColliders);
	}
	void ShowRestart()
	{
		FadeOut ();
		Menu.gameStatus = Menu.GameStatus.Gameplay;
		SceneManager.LoadScene ("gameplay");
		CommonFunctions.EnablePopUpColliders (false,PopUpColliders);
	}
	void AllSoundOn()
	{
		if (PlayerPrefs.GetInt ("GameSound") == 1) 
		{
			auClick.mute = false;
			auCoin.mute = false;
			auIn_Swap.mute = false;
			auOut_Swap.mute = false;
			auWin.mute = false;
			auKey.mute = false;
			auHit.mute = false;
			auStar.mute = false;
		}
	}
	void AllSoundOff()
	{
		if (PlayerPrefs.GetInt ("GameSound") == 0) 
		{
			auClick.mute = true;
			auCoin.mute = true;
			auIn_Swap.mute = true;
			auOut_Swap.mute = true;
			auWin.mute = true;
			auKey.mute = true;
			auHit.mute = true;
			auStar.mute = true;
		}
	}
	void CheckSound()
	{
		Menu.blnSound = (PlayerPrefs.GetInt ("GameSound") == 1 ? true : false);

		if(Menu.blnSound)
		{
			PopUpPaused.transform.Find("btnSound_On").GetComponent<SpriteRenderer>().sprite = SrtSfx[0];
			AllSoundOn ();
		}
		else
		{
			PopUpPaused.transform.Find("btnSound_On").GetComponent<SpriteRenderer>().sprite = SrtSfx[1];
			AllSoundOff ();
		}
	}

	void CheckMusic()
	{
		Menu.blnMusic = (PlayerPrefs.GetInt("GameMusic")==1? true:false);

		if(Menu.blnMusic)
		{
			PopUpPaused.transform.Find("btnMusic_On").GetComponent<SpriteRenderer>().sprite = SrtSfx[2];
			auBgMisic.mute = false;
		}
		else
		{
			PopUpPaused.transform.Find("btnMusic_On").GetComponent<SpriteRenderer>().sprite = SrtSfx[3];
			auBgMisic.mute = true;

		}
	}
		
	void ShowPopUp(GameObject PopUp)
	{
		iTween.RotateTo (PopUp, iTween.Hash ("z",0,"time", 0.2f, "easetype", iTween.EaseType.linear, "ignoretimescale", true));
		iTween.ScaleTo (PopUp, iTween.Hash ("x",1,"y",1,"time", 0.2f, "easetype", iTween.EaseType.linear, "ignoretimescale", true));
	}
	void HidePopUp(GameObject PopUp)
	{
		iTween.RotateTo (PopUp, iTween.Hash ("z",180,"time", 0.2f, "easetype", iTween.EaseType.linear, "ignoretimescale", true));
		iTween.ScaleTo (PopUp, iTween.Hash ("x",0,"y",0,"time", 0.2f, "easetype", iTween.EaseType.linear, "ignoretimescale", true,
													"oncomplete","Allcollideringameplay","oncompletetarget",gameObject));
	}

	float fadeInOutTime=1.0f;
	public void FadeOut()
	{
		iTween.FadeTo(goFadePlane,iTween.Hash("amount",0.0,"time",fadeInOutTime,"easetype",iTween.EaseType.linear,"ignoretimescale",true));
	}

	public void FadeIn(string method)
	{
		iTween.FadeTo(goFadePlane,iTween.Hash("amount",1.0,"time",fadeInOutTime,"easetype",iTween.EaseType.linear,"oncomplete",method,"oncompletetarget",gameObject,"ignoretimescale",true));
	}
	void animPunchScale(GameObject go,string fun,float scalevalue)
	{
		iTween.PunchScale(go,iTween.Hash("x",scalevalue,"y",scalevalue,"easetype",iTween.EaseType.easeOutBack,"time",0.5f,"ignoretimescale",true,"oncomplete",fun,"oncompletetarget",gameObject));
	}
	float OverfadeInOutTime=0.5f;
	public void OverFadeIn(string method)
	{
		iTween.FadeTo(goFadePlane,iTween.Hash("amount",1.0f,"time",OverfadeInOutTime,"easetype",iTween.EaseType.linear,"oncomplete",method,"oncompletetarget",gameObject,"ignoretimescale",true));
	}
	public void OverFadeOut()
	{
		iTween.FadeTo(goFadePlane,iTween.Hash("amount",0.0,"time",OverfadeInOutTime,"easetype",iTween.EaseType.linear,"ignoretimescale",true));
	}

	void OnApplicationPause (bool isGamePause)
	{
		if (isGamePause) 
		{
			_isGamePause = true;
		} 
	}
	void OnApplicationFocus (bool isGameFocus)
	{
		if (isLevelFail||isLevelClear)
				return;	

		if (isGameFocus && _isGamePause) 
		{
			_isGamePause = false;
				ShowPaused ();
		} 

	}

	private void HandleShowResult(ShowResult result)
	{
		Debug.Log("Done : "+result);

		if(result == ShowResult.Finished)
		{
			HidePopUp (PopUpLevelUnlock);
			if (LevelSelection.selectedLevel < CommonFunctions.totalLevels - 1) 
			{
				FadeIn ("SkipLevels");
			} 
			else if (LevelSelection.selectedLevel == CommonFunctions.totalLevels - 1) 
			{
				ShowPopUp (PopUpEndGames);
			}
		}	
	}
		
}
