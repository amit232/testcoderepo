﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityEngine.Advertisements;
using FlurrySDK;

public class Menu : MonoBehaviour
{
	public ObserverCollider[] allColliders;
	public ObserverCollider[] PopUpColliders;
	public GameObject[] Players,Player_Colors;
	public Color[] Choise_Colors;
	public Transform[] trLevelPaging;
	public Sprite[] Sptsfx;
	public GameObject goMenuUI,goWorldUI,goLevelUI,goTutorialsUI,goGameplayUI,goFadePlane,PopUpExit,PopUpStore,PopUpVideoAds,PopUpInternet_Connection,PopUpBg,PopUpPlayer_changeColor,Selected_Pos,btnPopupUnlock, popupAddCoins;
	public Transform trbtnSound, trbtnMusic,trbtnPlayer,trbtnNext,trbtnPrve;
	public TextMesh tmCoin,tmMsg,tmCoinAtMenu;
	public static bool blnSetting,blnMusic,blnSound,isPopUp;
	public static int selectedPlayer=0,selectedColors=0;
	public AudioSource auClick,auBgMisic;
	float fadeInOutTime=1.0f;
	public static int currPlayers=0,nextPlayers=0; 

	int cntCoins=0;
    private string FLURRY_API_KEY = "GS3FX89NBNKBBJF3DWKT";

	public enum GameStatus 
	{
		Menu=0,
		Player_Selection=1,
		Worlds=2,
		Levels=3,
		Tutorials=4,
		Gameplay=5
	};
	public static GameStatus gameStatus;
	public GameObject goPlayerMenu;
	void Awake()
	{
//		PlayerPrefs.DeleteAll ();

		

		if (!PlayerPrefs.HasKey ("level0world0locked") || !PlayerPrefs.HasKey ("Color0locked") || !PlayerPrefs.HasKey ("Player0locked")) 
				CommonFunctions.savePlayerData ();
				CommonFunctions.getPlayerData ();

		Time.timeScale = 1.0f;
		blnSetting = false;
		isPopUp = false;
		gameplay.isLeft = false;
		gameplay.isRight = false;
		gameplay.isLevelClear = false;
		gameplay.isStarGames = false;
		gameplay.isLevelFail = false;
		gameplay.isPaused = false;
		gameplay.isPopUp = false;
		gameplay._isGamePause = false;


		auBgMisic.Play ();

		switch(gameStatus)
		{
		case GameStatus.Menu:
			goMenuUI.SetActive (true);
			goLevelUI.SetActive (false);
			goGameplayUI.SetActive (false);
			goWorldUI.SetActive (false);
			goTutorialsUI.SetActive (false);
			break;
		case GameStatus.Worlds:
			goMenuUI.SetActive (false);
			goLevelUI.SetActive (false);
			goGameplayUI.SetActive (false);
			goWorldUI.SetActive (true);
			goTutorialsUI.SetActive (false);
			break;
		case GameStatus.Levels:
			goMenuUI.SetActive (false);
			goLevelUI.SetActive (true);
			goGameplayUI.SetActive (false);
			goWorldUI.SetActive (false);
			break;
		case GameStatus.Tutorials:
			goMenuUI.SetActive(false);
			goLevelUI.SetActive (false);
			goGameplayUI.SetActive(false);
			goWorldUI.SetActive (false);
			goTutorialsUI.SetActive (true);
			break;
		case GameStatus.Gameplay:
			goMenuUI.SetActive(false);
			goLevelUI.SetActive (false);
			goGameplayUI.SetActive(true);
			goWorldUI.SetActive (false);
			goTutorialsUI.SetActive (false);
			break;
		}	
		auBgMisic = GameObject.Find ("auBg").GetComponent<AudioSource>();

		if (gameStatus == GameStatus.Menu && CommonFunctions.CheckInternetConnection()) 
		{
			CommonFunctions.initAds ();
		}

        Social.localUser.Authenticate(success => {
            if (success)
            {
                Debug.Log("Authentication successful");
                string userInfo = "Username: " + Social.localUser.userName +
                                  "\nUser ID: " + Social.localUser.id +
                                  "\nIsUnderage: " + Social.localUser.underage;
                Debug.Log(userInfo);
            }
            else
                Debug.Log("Authentication failed");
        });



        new Flurry.Builder()
                  .WithCrashReporting(true)
                  .WithLogEnabled(true)
                  .WithLogLevel(Flurry.LogLevel.LogVERBOSE)
                  .WithMessaging(true)
                  .Build(FLURRY_API_KEY);
    }
		
	// Use this for initialization
	void Start ()
	{
		

		Screen.sleepTimeout = SleepTimeout.NeverSleep;

		foreach (ObserverCollider actCollider in allColliders) 
		{
			actCollider.TouchUp +=new ObserverColliderTouchEventHandler(ProcessButtonsUp);
			actCollider.TouchDown += new ObserverColliderTouchEventHandler (ProcessButtonsDown);
		}
		foreach (ObserverCollider actCollider in PopUpColliders)
		{
			actCollider.TouchDown += new ObserverColliderTouchEventHandler (PopupButtonsDown);
			actCollider.TouchUp += new ObserverColliderTouchEventHandler (PopupButtonsUp);
		}


		if(!PlayerPrefs.HasKey("GameSound"))
			PlayerPrefs.SetInt("GameSound",1);

		blnSound = (PlayerPrefs.GetInt("GameSound")==1? true:false);

		if(blnSound)
		{
			trbtnSound.gameObject.GetComponent<SpriteRenderer> ().sprite = Sptsfx [0];
			auClick.mute = false;
		} 
		else 
		{
			trbtnSound.gameObject.GetComponent<SpriteRenderer> ().sprite = Sptsfx [1];
			auClick.mute = true;
		}

		if(!PlayerPrefs.HasKey("GameMusic"))
			PlayerPrefs.SetInt("GameMusic",1);

		blnMusic = (PlayerPrefs.GetInt("GameMusic")==1? true:false);

		if(blnMusic)
		{
			trbtnMusic.gameObject.GetComponent<SpriteRenderer> ().sprite = Sptsfx [2];
			auBgMisic.mute = false;

			if (!auBgMisic.isPlaying)
				auBgMisic.mute = false;
				auBgMisic.Play ();
		} 
		else 
		{
			trbtnMusic.gameObject.GetComponent<SpriteRenderer> ().sprite = Sptsfx [3];
			auBgMisic.mute = true;
		}
			

		tmCoin.text = tmCoinAtMenu.text = PlayerPrefs.GetInt ("totalcoin").ToString();
		if (!PlayerPrefs.HasKey ("SelectedPlayers")) 
		{
			PlayerPrefs.SetInt ("SelectedPlayers", 0);
		} 
		else
		{
			selectedPlayer=PlayerPrefs.GetInt ("SelectedPlayers");
			currPlayers = PlayerPrefs.GetInt ("SelectedPlayers");

			btnPopupUnlock.SetActive (false);
			iTween.RotateTo (Players [PlayerPrefs.GetInt ("SelectedPlayers")], iTween.Hash ("z", 0, "time", 0.45f, "easetype", iTween.EaseType.linear, "ignoretimescale", true));
			iTween.ScaleTo (Players [PlayerPrefs.GetInt ("SelectedPlayers")], iTween.Hash ("x", 5, "y", 5, "time", 0.45f, "easetype", iTween.EaseType.linear, "ignoretimescale", true));

			Players [PlayerPrefs.GetInt ("SelectedPlayers")].GetComponent<SpriteRenderer> ().color = Choise_Colors [PlayerPrefs.GetInt ("SelectedColors")];
		}

		if (!PlayerPrefs.HasKey ("SelectedColors")) 
		{
			PlayerPrefs.SetInt ("SelectedColors", 0);
		}

		selectedColors = PlayerPrefs.GetInt ("SelectedColors");


		if(CommonFunctions.totalPlayers > 1)
			setNextPrevBtns (PlayerPrefs.GetInt ("SelectedPlayers"));

		SetPlayer ();
		InvokeRepeating("RotateTitle",6.0f,10.0f);

		if (gameStatus == GameStatus.Menu && CommonFunctions.CheckInternetConnection ()) 
		{
			CommonFunctions.ShowBannerAd ();
		}
	}

	int t=0;
	void RotateTitle() {

		if(t==0) {
			iTween.RotateTo(goPlayerMenu,iTween.Hash("z",9,"time",3.0f,"easetype",iTween.EaseType.linear));
			t=1;
		} 
		else if (t==1) {
			iTween.RotateTo(goPlayerMenu,iTween.Hash("z",-9,"time",3.0f,"easetype",iTween.EaseType.linear));
			t=0;
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKey (KeyCode.Escape) && !isPopUp) 
		{
			CommonFunctions.EnableColliders (false,allColliders);
			PopUpBg.SetActive (true);
			ShowPopUp(PopUpExit);
			isPopUp = true;
		}
	}

	public void ProcessButtonsDown(ObserverTouch source)
	{
		string buttonName = source.TouchCollider.name;
		Transform tr = source.TouchCollider.transform;

		auClick.Play ();

		 if (buttonName == "btnPlay") 
		{
            // Log Flurry events.
            Flurry.EventRecordStatus status = Flurry.LogEvent("Menu Screen");
            animPunchScale(tr.gameObject,"",0.5f);

			if (CommonFunctions.getGameDictionaryData ("Player" + currPlayers + "locked") == "yes" && CommonFunctions.getGameDictionaryData ("Color" + selectedColors + "locked") == "yes") 
			{
				
			}
			else if (CommonFunctions.getGameDictionaryData ("Player" + currPlayers + "locked") == "no" && CommonFunctions.getGameDictionaryData ("Color" + selectedColors + "locked") == "yes") 
			{
				PlayerPrefs.SetInt ("SelectedPlayers", currPlayers);
			} 
			else if (CommonFunctions.getGameDictionaryData ("Player" + currPlayers + "locked") == "yes" && CommonFunctions.getGameDictionaryData ("Color" + selectedColors + "locked") == "no") 
			{
				PlayerPrefs.SetInt ("SelectedColors", selectedColors);
			} 
			else 
			{
				PlayerPrefs.SetInt ("SelectedPlayers", currPlayers);
				PlayerPrefs.SetInt ("SelectedColors", selectedColors);
			}

		}
		else if (buttonName == "btnLeaderBorad") 
		{
			animPunchScale(tr.gameObject,"",0.3f);
		}
		else if (buttonName == "btnMusic") 
		{
			animPunchScale(tr.gameObject,"",0.3f);
		}
		else if (buttonName == "btnSound") 
		{
			animPunchScale(tr.gameObject,"",0.3f);
		}
		else if (buttonName == "btnPlayer") 
		{	
			isPopUp = true;
			CommonFunctions.EnableColliders (false,allColliders);
			animPunchScale(tr.gameObject,"",0.3f);

			if(CommonFunctions.totalPlayers > 1)
				setNextPrevBtns (PlayerPrefs.GetInt ("SelectedPlayers"));

			iTween.RotateTo (Players [currPlayers], iTween.Hash ("z", 180, "time", 0.2f, "easetype", iTween.EaseType.linear, "ignoretimescale", true));
			iTween.ScaleTo (Players [currPlayers], iTween.Hash ("x", 0, "y", 0, "time", 0.2f, "easetype", iTween.EaseType.linear, "ignoretimescale", true));

			Players [currPlayers].GetComponent<SpriteRenderer> ().color = Choise_Colors [PlayerPrefs.GetInt("SelectedColors")];

			selectedColors = PlayerPrefs.GetInt ("SelectedColors");

			if (PlayerPrefs.HasKey ("SelectedPlayers"))
			{
				currPlayers = PlayerPrefs.GetInt ("SelectedPlayers");

				btnPopupUnlock.SetActive (false);

				iTween.RotateTo (Players [PlayerPrefs.GetInt ("SelectedPlayers")], iTween.Hash ("z", 0, "time", 0.6f, "easetype", iTween.EaseType.linear, "ignoretimescale", true));
				iTween.ScaleTo (Players [PlayerPrefs.GetInt ("SelectedPlayers")], iTween.Hash ("x", 5, "y", 5, "time", 0.6f, "easetype", iTween.EaseType.linear, "ignoretimescale", true));

				Players [PlayerPrefs.GetInt ("SelectedPlayers")].GetComponent<SpriteRenderer> ().color = Choise_Colors [PlayerPrefs.GetInt ("SelectedColors")];
			} 
		}
	}
	public void ProcessButtonsUp(ObserverTouch source)
	{
		string buttonName = source.TouchCollider.name;

		 if (buttonName == "btnPlay") 
		{
			FadeIn ("ShowWorlds");
		}
		else if (buttonName == "btnLeaderBorad") 
		{
			Social.ShowLeaderboardUI ();
		}
		else if (buttonName == "btnMusic") 
		{
			MusicOnOff ();
		}
		else if (buttonName == "btnSound") 
		{
			SoundOnOff ();
		}
			
		else if (buttonName == "btnPlayer") 
		{
			PopUpBg.SetActive (true);
//			HideSetting ();
			ShowPopUp (PopUpPlayer_changeColor);
		}
	}
	public void PopupButtonsDown(ObserverTouch source)
	{
		
		string buttonName = source.TouchCollider.name;
		Transform tr = source.TouchCollider.transform;

		auClick.Play ();

		if (source.TouchCollider.tag == "tagColors") 
		{
            // Log Flurry events.
            Flurry.EventRecordStatus status = Flurry.LogEvent("Player Change Screen");
            animPunchScale (tr.gameObject, "", 0.3f);
		}
		else if (buttonName == "btnYes")
		{
			isPopUp = false;
			animPunchScale (tr.gameObject, "", 0.3f);
		} 
		else if (buttonName == "btnNo") 
		{
			isPopUp = false;
			PopUpBg.SetActive (false);
			animPunchScale (tr.gameObject, "", 0.3f);
			CommonFunctions.EnableColliders (true, allColliders);
		} 
		else if (buttonName == "btnVideoNo") 
		{
			animPunchScale (tr.gameObject, "", 0.3f);
		}
		else if (buttonName == "btnStoreNo") 
		{
			animPunchScale (tr.gameObject, "", 0.3f);
		} 
		else if (buttonName == "btnStoreYes") 
		{
			animPunchScale (tr.gameObject, "", 0.3f);
		} 
		else if (buttonName == "btnBack") 
		{
			CommonFunctions.EnableColliders (true, allColliders);
			animPunchScale (tr.gameObject, "", 0.3f);

		} 
		else if (buttonName == "btnFreeAds")
		{
			animPunchScale (tr.gameObject, "", 0.1f);
		} 
		else if (buttonName == "btnNext") 
		{
			animPunchScale (tr.gameObject, "", 0.3f);
		
		} 
		else if (buttonName == "btnPrve") 
		{
			animPunchScale (tr.gameObject, "", 0.3f);

		}
		else if (buttonName == "btnUnlock") 
		{
			animPunchScale (tr.gameObject, "", 0.3f);
		}
		else if (buttonName == "btnINCNO")
		{
			animPunchScale (tr.gameObject, "", 0.3f);
		}
		else if (buttonName == "btnAdsNo")
		{
			animPunchScale (tr.gameObject, "", 0.3f);
		}
        else if (buttonName == "btnAddCoinsClose")
        {
            animPunchScale(tr.gameObject, "", 0.3f);
        }
    }



	public void PopupButtonsUp(ObserverTouch source)
	{
		string buttonName = source.TouchCollider.name;

		 if (buttonName == "btnUnlock") 
		{
			if (200 > PlayerPrefs.GetInt ("totalcoin")) 
			{

			} 
			else 
			{
				PopUpBg.SetActive (true);
				ShowPopUp (PopUpStore);
				HidePopUp (PopUpPlayer_changeColor);
			}
		}

		else if (source.TouchCollider.tag == "tagColors") 
		{
			selectedColors = int.Parse (buttonName);

			SetLockedPlayer_Colors ();

		} 
		else if (buttonName == "btnNext") 
		{
			if(currPlayers < CommonFunctions.totalPlayers-1) 
			{
				nextPlayers = currPlayers;
				nextPlayers++;

				iTween.RotateTo (Players [currPlayers], iTween.Hash ("z", 180, "time", 0.2f, "islocal", true, "easetype", iTween.EaseType.linear, "ignoretimescale", true));
				iTween.ScaleTo (Players [currPlayers], iTween.Hash ("x", 0, "y", 0, "time", 0.2f, "easetype", iTween.EaseType.linear, "ignoretimescale", true,
					"oncomplete", "ShowNextPage", "oncompleteparams", nextPlayers, "oncompletetarget", gameObject));
	
			}

			StartCoroutine ("AllCollider");
				
		}
		else if (buttonName == "btnPrve") 
		{
			if(currPlayers > 0) 
			{
				nextPlayers = currPlayers;
				nextPlayers--;

				iTween.RotateTo (Players [currPlayers], iTween.Hash ("z", 180, "time", 0.2f, "islocal", true, "easetype", iTween.EaseType.linear, "ignoretimescale", true));
				iTween.ScaleTo (Players [currPlayers], iTween.Hash ("x", 0, "y", 0, "time", 0.2f, "easetype", iTween.EaseType.linear, "ignoretimescale", true,
					"oncomplete", "ShowNextPage", "oncompleteparams", nextPlayers, "oncompletetarget", gameObject));
			}

			StartCoroutine ("AllCollider");
		}
		else if (buttonName == "btnYes") 
		{
			Application.Quit ();
		}
		else if (buttonName == "btnNo") 
		{
			HidePopUp (PopUpExit);
		}
		else if (buttonName == "btnVideoNo") 
		{
			
		}
		else if (buttonName == "btnStoreNo") 
		{
			HidePopUp (PopUpStore);
			ShowPopUp (PopUpPlayer_changeColor);
		}
		else if (buttonName == "btnStoreYes") 
		{
			
			btnPopupUnlock.SetActive (false);

			ShowPopUp (PopUpPlayer_changeColor);
			HidePopUp (PopUpStore);


			if (CommonFunctions.getGameDictionaryData ("Player" + currPlayers + "locked") == "yes" && CommonFunctions.getGameDictionaryData ("Color" + selectedColors + "locked") == "yes") 
			{
				CommonFunctions.changeGameDictionary ("Player" + currPlayers + "locked", "no");
				CommonFunctions.changeGameDictionary ("Color" + selectedColors + "locked", "no");
			}
			else if (CommonFunctions.getGameDictionaryData ("Player" + currPlayers + "locked") == "no" && CommonFunctions.getGameDictionaryData ("Color" + selectedColors + "locked") == "yes") 
			{
				CommonFunctions.changeGameDictionary ("Color" + selectedColors + "locked", "no");
			} 
			else if (CommonFunctions.getGameDictionaryData ("Player" + currPlayers + "locked") == "yes" && CommonFunctions.getGameDictionaryData ("Color" + selectedColors + "locked") == "no") 
			{
				CommonFunctions.changeGameDictionary ("Player" + currPlayers + "locked", "no");
			} 
			else 
			{
		
			}

			int SubCoin = 0;
			SubCoin = PlayerPrefs.GetInt ("totalcoin") - 200;
			tmCoin.text = SubCoin.ToString ();
            // Log Flurry events.
            Flurry.EventRecordStatus status = Flurry.LogEvent("Player Change Screen-> Unlocked Player: "+ currPlayers + "locked");
            PlayerPrefs.SetInt ("totalcoin", SubCoin);
		}
		else if (buttonName == "btnBack") 
		{
			isPopUp = false;
			HidePopUp (PopUpPlayer_changeColor);
			StartCoroutine ("popBG");
		}
		else if (buttonName == "btnFreeAds") 
		{
			if (CommonFunctions.CheckInternetConnection ()) 
			{
				if (Advertisement.IsReady ()) 
				{
					var options = new ShowOptions { resultCallback = HandleShowResult };
					Advertisement.Show (null, options);
				} 
				else 
				{
					HidePopUp (PopUpPlayer_changeColor);
					ShowPopUp (PopUpVideoAds);
				}
			} 
			else 
			{
				HidePopUp (PopUpPlayer_changeColor);
				ShowPopUp (PopUpInternet_Connection);
			}
	
		}
		else if (buttonName == "btnINCNO") 
		{
			HidePopUp (PopUpInternet_Connection);
			ShowPopUp (PopUpPlayer_changeColor);
		}
		else if (buttonName == "btnAdsNo")
		{
			HidePopUp (PopUpVideoAds);
			ShowPopUp (PopUpPlayer_changeColor);
		}
        else if (buttonName == "btnAddCoinsClose")
        {
            HidePopUp(popupAddCoins);
            ShowPopUp(PopUpPlayer_changeColor);
        }
        

    }

	void setNextPrevBtns(int pgNo) 
	{
		if (pgNo == 0) 
		{
			trbtnPrve.gameObject.SetActive (false);
			trbtnNext.gameObject.SetActive (true);
		} 
		else if (pgNo == CommonFunctions.totalPlayers - 1) 
		{
			trbtnPrve.gameObject.SetActive (true);
			trbtnNext.gameObject.SetActive (false);
		} 
		else 
		{
			trbtnPrve.gameObject.SetActive (true);
			trbtnNext.gameObject.SetActive (true);
		}
	}
	IEnumerator popBG()
	{
		yield return new WaitForSeconds (0.23f);
		PopUpBg.SetActive (false);
	}
	void ShowNextPage(int playerNO)
	{	
		currPlayers = playerNO;

		iTween.RotateTo (Players[currPlayers], iTween.Hash ("z",0,"time", 0.2f, "easetype", iTween.EaseType.linear, "ignoretimescale", true));
		iTween.ScaleTo (Players[currPlayers], iTween.Hash ("x",5,"y",5,"time", 0.2f, "easetype", iTween.EaseType.linear, "ignoretimescale", true));

		SetLockedPlayer_Colors();

		setNextPrevBtns (currPlayers);
}
	public void SetLockedPlayer_Colors()
	{
		if (CommonFunctions.getGameDictionaryData ("Color" + selectedColors.ToString() + "locked") == "yes" && CommonFunctions.getGameDictionaryData("Player"+currPlayers.ToString()+"locked") == "yes" ) 
		{
			Players [currPlayers].GetComponent<SpriteRenderer> ().color = Choise_Colors [selectedColors];
			btnPopupUnlock.SetActive (true);
		}
		else if (CommonFunctions.getGameDictionaryData ("Color" + selectedColors.ToString() + "locked") == "no" && CommonFunctions.getGameDictionaryData("Player"+currPlayers.ToString()+"locked") == "yes" ) 
		{
			Players [currPlayers].GetComponent<SpriteRenderer> ().color = Choise_Colors [selectedColors];
			btnPopupUnlock.SetActive (true);
		}
		else if (CommonFunctions.getGameDictionaryData ("Color" + selectedColors.ToString() + "locked") == "yes" && CommonFunctions.getGameDictionaryData("Player"+currPlayers.ToString()+"locked") == "no" ) 
		{
			Players [currPlayers].GetComponent<SpriteRenderer> ().color = Choise_Colors [selectedColors];
			btnPopupUnlock.SetActive (true);
		}
		else
		{
			Players [currPlayers].GetComponent<SpriteRenderer> ().color = Choise_Colors [selectedColors];	
			btnPopupUnlock.SetActive (false);
		}
	}
		
	void ShowWorlds()
	{
		FadeOut ();
		gameStatus = GameStatus.Worlds;
		SceneManager.LoadScene ("gameplay");
	}
	void SoundOnOff()
	{
		if(blnSound)
		{
			blnSound =false;
			trbtnSound.gameObject.GetComponent<SpriteRenderer>().sprite = Sptsfx[1];
			PlayerPrefs.SetInt ("GameSound",0);
			auClick.mute = true;
		}
		else
		{
			blnSound = true;
			trbtnSound.gameObject.GetComponent<SpriteRenderer>().sprite = Sptsfx[0];
			PlayerPrefs.SetInt ("GameSound",1);
			auClick.mute=false;
		}
	}
	void MusicOnOff()
	{
		if(blnMusic)
		{
			trbtnMusic.gameObject.GetComponent<SpriteRenderer>().sprite = Sptsfx[3];
			blnMusic =false;
			PlayerPrefs.SetInt ("GameMusic",0);
			auBgMisic.mute=true;
		}
		else
		{
			blnMusic = true;
			trbtnMusic.gameObject.GetComponent<SpriteRenderer>().sprite = Sptsfx[2];
			PlayerPrefs.SetInt ("GameMusic",1);
			auBgMisic.mute=false;

			if (!auBgMisic.isPlaying)
				auBgMisic.mute = false;
		}
	}
		
	void ShowPopUp(GameObject PopUp)
	{
		iTween.RotateTo (PopUp, iTween.Hash ("z",0,"time", 0.2f, "easetype", iTween.EaseType.linear, "ignoretimescale", true));
		iTween.ScaleTo (PopUp, iTween.Hash ("x",1,"y",1,"time", 0.2f, "easetype", iTween.EaseType.linear, "ignoretimescale", true));
	}
	void HidePopUp(GameObject PopUp)
	{
		iTween.RotateTo (PopUp, iTween.Hash ("z",180,"time", 0.2f, "easetype", iTween.EaseType.linear, "ignoretimescale", true));
		iTween.ScaleTo (PopUp, iTween.Hash ("x",0,"y",0,"time", 0.2f, "easetype", iTween.EaseType.linear, "ignoretimescale", true));
	}

	void SetPlayer()
	{

		if (CommonFunctions.getGameDictionaryData ("Player" + currPlayers +"locked") == "yes" && CommonFunctions.getGameDictionaryData ("Color" + selectedColors +"locked") == "yes") 
		{
			btnPopupUnlock.SetActive (true);
		} 
		else if (CommonFunctions.getGameDictionaryData ("Player" + currPlayers +"locked") == "yes" && CommonFunctions.getGameDictionaryData ("Color" + selectedColors +"locked") == "no") 
		{
			btnPopupUnlock.SetActive (true);
		} 
		else if (CommonFunctions.getGameDictionaryData ("Player" + currPlayers +"locked") == "no" && CommonFunctions.getGameDictionaryData ("Color" + selectedColors +"locked") == "yes") 
		{
			btnPopupUnlock.SetActive (true);
		}
		else 
		{
			btnPopupUnlock.SetActive (false);
		}

	}
	void animPunchScale(GameObject go,string fun,float scalevalue)
	{
		iTween.PunchScale(go,iTween.Hash("x",scalevalue,"y",scalevalue,"easetype",iTween.EaseType.easeOutBack,"time",0.5f,"ignoretimescale",true,"oncomplete",fun,"oncompletetarget",gameObject));
	}
	public void FadeOut()
	{
		iTween.FadeTo(goFadePlane,iTween.Hash("amount",0.0,"time",fadeInOutTime,"easetype",iTween.EaseType.linear,"ignoretimescale",true));
	}

	public void FadeIn(string method)
	{
		CommonFunctions.EnableColliders (false,allColliders);
		iTween.FadeTo(goFadePlane,iTween.Hash("amount",1.0,"time",fadeInOutTime,"easetype",iTween.EaseType.linear,"oncomplete",method,"oncompletetarget",gameObject,"ignoretimescale",true));
	}

	IEnumerator AllCollider()
	{
		CommonFunctions.EnablePopUpColliders (false,PopUpColliders);
		yield return new WaitForSeconds (0.7f);
		CommonFunctions.EnablePopUpColliders (true,PopUpColliders);
	}

	private void HandleShowResult(ShowResult result)
	{
		Debug.Log("Done : "+result);
		if(result == ShowResult.Finished)
		{
			cntCoins = 50;
			PlayerPrefs.SetInt ("totalcoin", PlayerPrefs.GetInt("totalcoin")+cntCoins);
			tmCoin.text = tmCoinAtMenu.text = PlayerPrefs.GetInt ("totalcoin").ToString ();

            HidePopUp(PopUpPlayer_changeColor);
            ShowPopUp(popupAddCoins);

            // Log Flurry events.
            Flurry.EventRecordStatus status = Flurry.LogEvent("Get Free Coins");
        }	
	}
}
