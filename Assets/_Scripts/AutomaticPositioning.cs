﻿using UnityEngine;
using System.Collections;

public class AutomaticPositioning : MonoBehaviour
{
	public bool IsProportionatePositioningEnabled = true;
	private Vector2 BaseDeviceAspect;
	public Camera UICamera;
	float OrigPos = -99999f;
	bool IsObjectPositionSet = false;
	public bool isRunAutomatic = true;
	
	void Start ()
	{
		if(!isRunAutomatic)
			return;
		
		SetPos ();
	}
	
	void OnEnable ()
	{
		if(!isRunAutomatic)
			return;
		
		if (OrigPos == -99999f) 
		{
			
			SetPos ();		
		}
	}
	
	// Use this for initialization
	public void SetPos ()
	{
		if (IsObjectPositionSet) 
		{
			return;		
		}
		
		if (UICamera == null) 
		{
			UICamera = GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<Camera>();		
		}
		if (!IsProportionatePositioningEnabled) {
			return;		
		}
		if (UICamera.aspect < 1) {
			BaseDeviceAspect = new Vector2 (9, 16);		
		} else {
			BaseDeviceAspect = new Vector2 (4, 3);		
		}
		float baseAspect = ((float)BaseDeviceAspect.x) / ((float)BaseDeviceAspect.y);
		float BaseWorldWidth = 0;

		if (UICamera.aspect < 1)
		{
			if (baseAspect < 0.6f) 
			{
				BaseWorldWidth = 5.622817f;
			} 
			else if (baseAspect < 0.685f) 
			{
				BaseWorldWidth = 6.670547f;
			} 
			else if (baseAspect < 0.8f) 
			{
				BaseWorldWidth = 7.49709f;
				Debug.Log ("123");
			}
			Debug.Log ("231");
		} 
		else 
		{
			if (baseAspect < 1.45f) 
			{
				BaseWorldWidth = 13.32068f;
				Debug.Log ("00000");
			}
			else if (baseAspect < 1.55f) 
			{
				BaseWorldWidth = 15f;
				Debug.Log ("000000");
			} 
			else 
			{
				BaseWorldWidth = 17.77215f;
				Debug.Log ("0000000");
			}
			Debug.Log ("0");
		}
		float CameraSizeDifferencePercentage = (5f - UICamera.orthographicSize) / 5f;
		BaseWorldWidth = BaseWorldWidth - (CameraSizeDifferencePercentage * BaseWorldWidth);
		float ScreenWidthInWorld = UICamera.ScreenToWorldPoint (new Vector3 (Screen.width, 0, 1)).x - UICamera.ScreenToWorldPoint (new Vector3 (0, 0, 1)).x;
		float diff = 0f;
		if (transform.parent == null) {
			if (transform.position.x < 0) {
				diff = transform.position.x - (-BaseWorldWidth / 2f);
				transform.position = new Vector3 ((-ScreenWidthInWorld / 2f) + diff, transform.position.y, transform.position.z);
			} else if (transform.position.x > 0) {
				diff = (BaseWorldWidth / 2f) - transform.position.x;
				transform.position = new Vector3 ((ScreenWidthInWorld / 2f) - diff, transform.position.y, transform.position.z);
			}
		} else {
			if (transform.localPosition.x < 0) {
				diff = transform.localPosition.x - (-BaseWorldWidth / 2f);
				transform.localPosition = new Vector3 ((-ScreenWidthInWorld / 2f) + diff, transform.localPosition.y, transform.localPosition.z);
			} else if (transform.localPosition.x > 0) {
				diff = (BaseWorldWidth / 2f) - transform.localPosition.x;
				transform.localPosition = new Vector3 ((ScreenWidthInWorld / 2f) - diff, transform.localPosition.y, transform.localPosition.z);
			}
		}
		OrigPos = diff;
		
		if(isRunAutomatic)
			IsObjectPositionSet = true;
	}	
	
}
