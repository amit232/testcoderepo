﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Tutorials_Scripts : MonoBehaviour 
{
	public ObserverCollider[] allColliders;
	public GameObject goFadePlane, PopupTutorials;
	public AudioSource auClick,auBgMisic;
	float fadeInOutTime=1.0f;
	int isShowTutorials=1;



	void Awake()
	{
		auBgMisic = GameObject.Find ("auBg").GetComponent<AudioSource>();
	}
	// Use this for initialization
	void Start () 
	{
		foreach (ObserverCollider actCollider in allColliders) 
		{
			actCollider.TouchUp +=new ObserverColliderTouchEventHandler(ProcessButtonsUp);
			actCollider.TouchDown += new ObserverColliderTouchEventHandler (ProcessButtonsDown);
		}
		if (!Menu.blnSound) 
		{
			auClick.mute = true;
		} 
		else 
		{
			auClick.mute = false;
		}

		if (!Menu.blnMusic) 
		{
			auBgMisic.mute = true;
		} 
		else 
		{
			auBgMisic.mute = false;

			if (!auBgMisic.isPlaying) 
			{
				auBgMisic.mute = false;
				auBgMisic.Play ();
			}
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void ProcessButtonsDown(ObserverTouch source) 
	{
		string buttonName = source.TouchCollider.name;
		Transform tr = source.TouchCollider.transform;
		auClick.Play ();

		if (buttonName == "btnGO") 
		{
			animPunchScale(tr.gameObject,"",0.5f);
		}
	}
	public void ProcessButtonsUp(ObserverTouch source)
	{
		string buttonName = source.TouchCollider.name;

		if (buttonName == "btnGO") 
		{
			FadeIn ("ShowGamePlay");
		}
	}

	public void FadeOut()
	{
		iTween.FadeTo(goFadePlane,iTween.Hash("amount",0.0,"time",fadeInOutTime,"easetype",iTween.EaseType.linear,"ignoretimescale",true));
	}

	public void FadeIn(string method)
	{
		iTween.FadeTo(goFadePlane,iTween.Hash("amount",1.0,"time",fadeInOutTime,"easetype",iTween.EaseType.linear,"oncomplete",method,"oncompletetarget",gameObject,"ignoretimescale",true));
	}
	void animPunchScale(GameObject go,string fun,float scalevalue)
	{
		iTween.PunchScale(go,iTween.Hash("x",scalevalue,"y",scalevalue,"easetype",iTween.EaseType.easeOutBack,"time",0.5f,"ignoretimescale",true,"oncomplete",fun,"oncompletetarget",gameObject));
	}

	public void ShowTutorials()
	{
		iTween.RotateTo (PopupTutorials, iTween.Hash ("z",180,"time", 0.2f, "easetype", iTween.EaseType.linear, "ignoretimescale", true));
		iTween.ScaleTo (PopupTutorials, iTween.Hash ("x",0,"y",0,"time", 0.2f, "easetype", iTween.EaseType.linear, "ignoretimescale", true));
	}

	void ShowGamePlay()
	{
		isShowTutorials = 1;
		PlayerPrefs.SetInt ("SetTutorials",PlayerPrefs.GetInt("SetTutorials")+isShowTutorials);
		FadeOut ();
		Menu.gameStatus = Menu.GameStatus.Gameplay;
		SceneManager.LoadScene ("gameplay");
	}

}
