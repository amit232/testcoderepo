﻿using UnityEngine;
using System.Collections;

public class scaleupdown : MonoBehaviour {

	public float scalevalue;
	public float delaytime=2;

	// Use this for initialization
	void Start () 
	{
		iTween.ScaleTo(this.gameObject,iTween.Hash("x",scalevalue,"y",scalevalue,"z",scalevalue,"delay",delaytime,"easetype",iTween.EaseType.linear,"looptype",iTween.LoopType.pingPong));
		
		if(this.gameObject.tag == "tagSpike")
			InvokeRepeating("colliderscale",0.0f,0.1f);
	}
	
	// Update is called once per frame
	void Update () {

			

	}

	void colliderscale()
	{
		if(this.transform.localScale.x > 0.15f)
			this.GetComponent<CircleCollider2D>().enabled = true;
		else if (this.transform.localScale.x == 0.0f)
			this.GetComponent<CircleCollider2D>().enabled = false;
	}
}
