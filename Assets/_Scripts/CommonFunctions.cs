using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Xml;

using admob;
using UnityEngine.Advertisements;
using FlurrySDK;
using ChartboostSDK;

public class CommonFunctions : MonoBehaviour {

	public static int AdTurn = 0;
	public static int totalPlayers = 8;
	public static int totalLevels = 50,totalColors=10;
	public static int totalWorlds = 2;
	public static int totalLevelPages = 5;

    public static string HighScoresId = "rollinghero.highscore";
    //Apple ID "1142106736";

    #region  --Letang Monetization Game Ids--
    /* Ids
    //Letang Unity Ads ID, AdMob, Vungle, FB
    private static string UnityAdsId = "3207236";//"2540868";

    //AdMob
    private static string AdMobAppId = "ca-app-pub-7006207450114764~9063501102";
    private static string AdMobInsterstitialID = "ca-app-pub-7006207450114764/3811174429"; //"ca-app-pub-1735139870435418/4552583887";
    private static string AdMobVideoID = "ca-app-pub-7006207450114764/7610393605"; //"ca-app-pub-1735139870435418/4552583887";
                                                                                // private static string AdMobBannerId = "ca-app-pub-1735139870435418/3075850686";

    //Vungle
    static string VungleappID = "5d1af27bc92b860018fba847";
    static string VunglePlacementID1 = "DEFAULT-2056637";
    static string VunglePlacementID2 = "REWARD-1209087";


    */
    #endregion

    #region  --Macrobian Monetization Game Ids--

    //Unity Ads ID
    private static string UnityAdsId = "3353274";

    //AdMob
    private static string AdMobAppId = "ca-app-pub-1735139870435418~1599117482";
    private static string AdMobInsterstitialID = "ca-app-pub-1735139870435418/4552583887";


    //Vungle STOP on 15.11
    //static string VungleappID = "5dc2661deaf63400121cb9ec";
    //static string VunglePlacementID1 = "DEFAULT-0325193"; // FullScreen
    //static string VunglePlacementID2 = "REWARD-6794374";

    //ChartBoost ID
//      App ID: 5dce4d4dff68030a17997f68
//      App Signature : eee01fa0af5228c113cabea783b288712cca8633

    #endregion

    private static Dictionary<string, string> gameDictionary = new Dictionary<string, string>() 
	{

		{"Player0locked","no"},
		{"Player0unlockvalue","200"},
		{"Player0name","BOMBATO"},

		{"Player1locked","yes"},
		{"Player1unlockvalue","200"},
		{"Player1name","LUSOTO"},

		{"Player2locked","yes"},
		{"Player2unlockvalue","200"},
		{"Player2name","BHANSO"},

		{"Player3locked","yes"},
		{"Player3unlockvalue","200"},
		{"Player3name","KULO-TAL"},

		{"Player4locked","yes"},
		{"Player4unlockvalue","200"},
		{"Player4name","PANDO"},

		{"Player5locked","yes"},
		{"Player5unlockvalue","200"},
		{"Player5name","PANDO"},

		{"Player6locked","yes"},
		{"Player6unlockvalue","200"},
		{"Player6name","PANDO"},

		{"Player7locked","yes"},
		{"Player7unlockvalue","200"},
		{"Player7name","PANDO"},


		{"Color0locked","no"},
		{"Color1locked","yes"},
		{"Color2locked","yes"},
		{"Color3locked","yes"},
		{"Color4locked","yes"},
		{"Color5locked","yes"},
		{"Color6locked","yes"},
		{"Color7locked","yes"},
		{"Color8locked","yes"},
		{"Color9locked","yes"},


		{"level0time","22"},
		{"level0score","0"},
		{"level0world0star","0"},
		{"level0world1star","0"},
		{"level0world0locked","no"},
		{"level0world1locked","no"},
		{"level0UnlockCounter","0"},

		{"level1time","20"},
		{"level1score","0"},
		{"level1world0star","0"},
		{"level1world1star","0"},
		{"level1world0locked","yes"},
		{"level1world1locked","yes"},
		{"level1UnlockCounter","0"},
	

		{"level2time","17"},
		{"level2score","0"},
		{"level2world0star","0"},
		{"level2world1star","0"},
		{"level2world0locked","yes"},
		{"level2world1locked","yes"},
		{"level2UnlockCounter","0"},

		{"level3time","14"},	
		{"level3score","0"},
		{"level3world0star","0"},
		{"level3world1star","0"},
		{"level3world0locked","yes"},
		{"level3world1locked","yes"},
		{"level3UnlockCounter","0"},

		{"level4time","20"},
		{"level4score","0"},
		{"level4world0star","0"},
		{"level4world1star","0"},
		{"level4world0locked","yes"},
		{"level4world1locked","yes"},
		{"level4UnlockCounter","0"},

		{"level5time","22"},
		{"level5score","0"},
		{"level5world0star","0"},
		{"level5world1star","0"},
		{"level5world0locked","yes"},
		{"level5world1locked","yes"},
		{"level5UnlockCounter","0"},

		{"level6time","21"},
		{"level6score","0"},
		{"level6world0star","0"},
		{"level6world1star","0"},
		{"level6world0locked","yes"},
		{"level6world1locked","yes"},
		{"level6UnlockCounter","0"},

		{"level7time","32"},
		{"level7score","0"},
		{"level7world0star","0"},
		{"level7world1star","0"},
		{"level7world0locked","yes"},
		{"level7world1locked","yes"},
		{"level7UnlockCounter","0"},

		{"level8time","45"},
		{"level8score","0"},
		{"level8world0star","0"},
		{"level8world1star","0"},
		{"level8world0locked","yes"},
		{"level8world1locked","yes"},
		{"level8UnlockCounter","0"},

		{"level9time","30"},
		{"level9score","0"},
		{"level9world0star","0"},
		{"level9world1star","0"},
		{"level9world0locked","yes"},
		{"level9world1locked","yes"},
		{"level9UnlockCounter","0"},

		{"level10time","42"},
		{"level10score","0"},
		{"level10world0star","0"},
		{"level10world1star","0"},
		{"level10world0locked","yes"},
		{"level10world1locked","yes"},
		{"level10UnlockCounter","0"},

		{"level11time","25"},
		{"level11score","0"},
		{"level11world0star","0"},
		{"level11world1star","0"},
		{"level11world0locked","yes"},
		{"level11world1locked","yes"},
		{"level11UnlockCounter","0"},

		{"level12time","32"},
		{"level12score","0"},
		{"level12world0star","0"},
		{"level12world1star","0"},
		{"level12world0locked","yes"},
		{"level12world1locked","yes"},
		{"level12UnlockCounter","0"},

		{"level13time","67"},
		{"level13score","0"},
		{"level13world0star","0"},
		{"level13world1star","0"},
		{"level13world0locked","yes"},
		{"level13world1locked","yes"},
		{"level13UnlockCounter","0"},

		{"level14time","65"},
		{"level14score","0"},
		{"level14world0star","0"},
		{"level14world1star","0"},
		{"level14world0locked","yes"},
		{"level14world1locked","yes"},
		{"level14UnlockCounter","0"},

		{"level15time","48"},
		{"level15score","0"},
		{"level15world0star","0"},
		{"level15world1star","0"},
		{"level15world0locked","yes"},
		{"level15world1locked","yes"},
		{"level15UnlockCounter","0"},

		{"level16time","70"},
		{"level16score","0"},
		{"level16world0star","0"},
		{"level16world1star","0"},
		{"level16world0locked","yes"},
		{"level16world1locked","yes"},
		{"level16UnlockCounter","0"},

		{"level17time","55"},
		{"level17score","0"},
		{"level17world0star","0"},
		{"level17world1star","0"},
		{"level17world0locked","yes"},
		{"level17world1locked","yes"},
		{"level17UnlockCounter","0"},

		{"level18time","50"},
		{"level18score","0"},
		{"level18world0star","0"},
		{"level18world1star","0"},
		{"level18world0locked","yes"},
		{"level18world1locked","yes"},
		{"level18UnlockCounter","0"},

		{"level19time","55"},
		{"level19score","0"},
		{"level19world0star","0"},
		{"level19world1star","0"},
		{"level19world0locked","yes"},
		{"level19world1locked","yes"},
		{"level19UnlockCounter","0"},

		{"level20time","95"},
		{"level20score","0"},
		{"level20world0star","0"},
		{"level20world1star","0"},
		{"level20world0locked","yes"},
		{"level20world1locked","yes"},
		{"level20UnlockCounter","0"},

		{"level21time","45"},
		{"level21score","0"},
		{"level21world0star","0"},
		{"level21world1star","0"},
		{"level21world0locked","yes"},
		{"level21world1locked","yes"},
		{"level21UnlockCounter","0"},

		{"level22time","45"},
		{"level22score","0"},
		{"level22world0star","0"},
		{"level22world1star","0"},
		{"level22world0locked","yes"},
		{"level22world1locked","yes"},
		{"level22UnlockCounter","0"},

		{"level23time","45"},
		{"level23score","0"},
		{"level23world0star","0"},
		{"level23world1star","0"},
		{"level23world0locked","yes"},
		{"level23world1locked","yes"},
		{"level23UnlockCounter","0"},

		{"level24time","40"},
		{"level24score","0"},
		{"level24world0star","0"},
		{"level24world1star","0"},
		{"level24world0locked","yes"},
		{"level24world1locked","yes"},
		{"level24UnlockCounter","0"},

		{"level25time","55"},
		{"level25score","0"},
		{"level25world0star","0"},
		{"level25world1star","0"},
		{"level25world0locked","yes"},
		{"level25world1locked","yes"},
		{"level25UnlockCounter","0"},

		{"level26time","55"},
		{"level26score","0"},
		{"level26world0star","0"},
		{"level26world1star","0"},
		{"level26world0locked","yes"},
		{"level26world1locked","yes"},
		{"level26UnlockCounter","0"},

		{"level27time","50"},
		{"level27score","5000"},
		{"level27world0star","0"},
		{"level27world1star","0"},
		{"level27world0locked","yes"},
		{"level27world1locked","yes"},
		{"level27UnlockCounter","0"},

		{"level28time","50"},
		{"level28score","0"},
		{"level28world0star","0"},
		{"level28world1star","0"},
		{"level28world0locked","yes"},
		{"level28world1locked","yes"},
		{"level28UnlockCounter","0"},

		{"level29time","55"},
		{"level29score","0"},
		{"level29world0star","0"},
		{"level29world1star","0"},
		{"level29world0locked","yes"},
		{"level29world1locked","yes"},
		{"level29UnlockCounter","0"},

		{"level30time","65"},
		{"level30score","0"},
		{"level30world0star","0"},
		{"level30world1star","0"},
		{"level30world0locked","yes"},
		{"level30world1locked","yes"},
		{"level30UnlockCounter","0"},

		{"level31time","45"},
		{"level31score","0"},
		{"level31world0star","0"},
		{"level31world1star","0"},
		{"level31world0locked","yes"},
		{"level31world1locked","yes"},
		{"level31UnlockCounter","0"},

		{"level32time","75"},
		{"level32score","0"},
		{"level32world0star","0"},
		{"level32world1star","0"},
		{"level32world0locked","yes"},
		{"level32world1locked","yes"},
		{"level32UnlockCounter","0"},

		{"level33time","45"},
		{"level33score","0"},
		{"level33world0star","0"},
		{"level33world1star","0"},
		{"level33world0locked","yes"},
		{"level33world1locked","yes"},
		{"level33UnlockCounter","0"},

		{"level34time","50"},
		{"level34score","0"},
		{"level34world0star","0"},
		{"level34world1star","0"},
		{"level34world0locked","yes"},
		{"level34world1locked","yes"},
		{"level34UnlockCounter","0"},

		{"level35time","50"},
		{"level35score","0"},
		{"level35world0star","0"},
		{"level35world1star","0"},
		{"level35world0locked","yes"},
		{"level35world1locked","yes"},
		{"level35UnlockCounter","0"},


		{"level36time","50"},
		{"level36score","0"},
		{"level36world0star","0"},
		{"level36world1star","0"},
		{"level36world0locked","yes"},
		{"level36world1locked","yes"},
		{"level36UnlockCounter","0"},

		{"level37time","45"},
		{"level37score","0"},
		{"level37world0star","0"},
		{"level37world1star","0"},
		{"level37world0locked","yes"},
		{"level37world1locked","yes"},
		{"level37UnlockCounter","0"},

		{"level38time","60"},
		{"level38score","0"},
		{"level38world0star","0"},
		{"level38world1star","0"},
		{"level38world0locked","yes"},
		{"level38world1locked","yes"},
		{"level38UnlockCounter","0"},

		{"level39time","50"},
		{"level39score","0"},
		{"level39world0star","0"},
		{"level39world1star","0"},
		{"level39world0locked","yes"},
		{"level39world1locked","yes"},
		{"level39UnlockCounter","0"},

		{"level40time","50"},
		{"level40score","0"},
		{"level40world0star","0"},
		{"level40world1star","0"},
		{"level40world0locked","yes"},
		{"level40world1locked","yes"},
		{"level40UnlockCounter","0"},

		{"level41time","60"},
		{"level41score","0"},
		{"level41world0star","0"},
		{"level41world1star","0"},
		{"level41world0locked","yes"},
		{"level41world1locked","yes"},
		{"level41UnlockCounter","0"},

		{"level42time","25"},
		{"level42score","0"},
		{"level42world0star","0"},
		{"level42world1star","0"},
		{"level42world0locked","yes"},
		{"level42world1locked","yes"},
		{"level42UnlockCounter","0"},

		{"level43time","45"},
		{"level43score","0"},
		{"level43world0star","0"},
		{"level43world1star","0"},
		{"level43world0locked","yes"},
		{"level43world1locked","yes"},
		{"level43UnlockCounter","0"},

		{"level44time","60"},
		{"level44score","0"},
		{"level44world0star","0"},
		{"level44world1star","0"},
		{"level44world0locked","yes"},
		{"level44world1locked","yes"},
		{"level44UnlockCounter","0"},

		{"level45time","80"},
		{"level45score","0"},
		{"level45world0star","0"},
		{"level45world1star","0"},
		{"level45world0locked","yes"},
		{"level45world1locked","yes"},
		{"level45UnlockCounter","0"},

		{"level46time","60"},
		{"level46score","0"},
		{"level46world0star","0"},
		{"level46world1star","0"},
		{"level46world0locked","yes"},
		{"level46world1locked","yes"},
		{"level46UnlockCounter","0"},

		{"level47time","60"},
		{"level47score","0"},
		{"level47world0star","0"},
		{"level47world1star","0"},
		{"level47world0locked","yes"},
		{"level47world1locked","yes"},
		{"level47UnlockCounter","0"},

		{"level48time","50"},
		{"level48score","0"},
		{"level48world0star","0"},
		{"level48world1star","0"},
		{"level48world0locked","yes"},
		{"level48world1locked","yes"},
		{"level48UnlockCounter","0"},

		{"level49time","65"},
		{"level49score","0"},
		{"level49world0star","0"},
		{"level49world1star","0"},
		{"level49world0locked","yes"},
		{"level49world1locked","yes"},
		{"level49UnlockCounter","0"},


    };

	
	public static string getGameDictionaryData(string levelKey)
	{
		return gameDictionary[levelKey];
	}
		
	public static void changeGameDictionary(string key, string val)
	{
		gameDictionary.Remove(key);
		gameDictionary.Add(key,val);
		
		PlayerPrefs.SetString(key,val);
	}
	
	public static void savePlayerData()
	{

		for (int i = 0; i < totalLevels; i++)
		{
			for (int j=0; j < totalWorlds; j++) 
			{
				PlayerPrefs.SetString ("level" + i.ToString () +"world" + j.ToString()+ "locked", gameDictionary ["level" + i.ToString () +"world" + j.ToString()+ "locked"]);
				PlayerPrefs.SetString("level"+i.ToString()+"world"+j.ToString()+"star",gameDictionary["level"+i.ToString()+"world"+j.ToString()+"star"]);
			}
		}

		for (int i = 0; i < totalLevels; i++) 
		{
			PlayerPrefs.SetString ("level"+i.ToString()+"time",gameDictionary["level"+i.ToString()+"time"]);
			PlayerPrefs.SetString ("level"+i.ToString()+"score",gameDictionary["level"+i.ToString()+"score"]);
			PlayerPrefs.SetString ("level"+i.ToString()+"UnlockCounter",gameDictionary["level"+i.ToString()+"UnlockCounter"]);
		}

	
		for (int i=0; i < totalColors; i++) 
		{
				PlayerPrefs.SetString ("Color" + i.ToString()+ "locked", gameDictionary ["Color" + i.ToString()+ "locked"]);
		}


		for (int i = 0; i < totalPlayers; i++) 
		{
			PlayerPrefs.SetString ("Player" + i.ToString () +"locked", gameDictionary ["Player" + i.ToString () +"locked"]);
		}
	}
	
	public static void getPlayerData()
	{
		for(int i = 0; i < totalLevels; i++)
		{
			for(int j=0; j< totalWorlds; j++)
			{
				gameDictionary.Remove("level"+i.ToString()+"world" + j.ToString()+"locked");
				gameDictionary.Add("level"+i.ToString()+"world" + j.ToString()+"locked",PlayerPrefs.GetString("level"+i.ToString()+"world" + j.ToString()+"locked"));

				gameDictionary.Remove("level"+i.ToString()+"world"+j.ToString()+"star");
				gameDictionary.Add("level"+i.ToString()+"world"+j.ToString()+"star",PlayerPrefs.GetString("level"+i.ToString()+"world"+j.ToString()+"star"));
			}
		}

		for (int i = 0; i < totalLevels; i++) 
		{
			gameDictionary.Remove ("level"+i.ToString()+"time");
			gameDictionary.Add ("level"+i.ToString()+"time",PlayerPrefs.GetString("level"+i.ToString()+"time"));

			gameDictionary.Remove ("level"+i.ToString()+"score");
			gameDictionary.Add ("level"+i.ToString()+"score",PlayerPrefs.GetString("level"+i.ToString()+"score"));

			gameDictionary.Remove ("level"+i.ToString()+"UnlockCounter");
			gameDictionary.Add ("level"+i.ToString()+"UnlockCounter",PlayerPrefs.GetString("level"+i.ToString()+"UnlockCounter"));
		}

	
		for (int i = 0; i < totalPlayers; i++) 
		{
			gameDictionary.Remove("Player"+i.ToString()+"locked");
			gameDictionary.Add("Player"+i.ToString()+"locked",PlayerPrefs.GetString("Player"+i.ToString()+"locked"));
		}

		for (int i = 0; i < totalColors; i++) 
		{
			gameDictionary.Remove("Color"+i.ToString()+"locked");
			gameDictionary.Add("Color"+i.ToString()+"locked",PlayerPrefs.GetString("Color"+i.ToString()+"locked"));
		}


	}
	public static void EnableColliders(bool isEnable, ObserverCollider[] allColliders)
	{
		foreach (ObserverCollider actCollider in allColliders) {
			actCollider.GetComponent<Collider>().enabled = isEnable;
		}
	}
	public static void EnablePopUpColliders(bool isEnable, ObserverCollider[] PopUpColliders)
	{
		foreach (ObserverCollider actCollider in PopUpColliders) 
		{
			actCollider.GetComponent<Collider>().enabled = isEnable;
		}
	}


	public static bool CheckInternetConnection()
	{
		bool isConnectedToInternet = false;

		if(Application.internetReachability == NetworkReachability.ReachableViaCarrierDataNetwork || 
			Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork) {
			isConnectedToInternet = true;
		}

		return isConnectedToInternet;
	}

    static Admob ad;
    static int AdCount = 0;

    public static void initAds()
	{
		if(Application.platform == RuntimePlatform.IPhonePlayer)
		{
            AdProperties adProperties = new AdProperties();
            adProperties.isTesting = false;

            ad = Admob.Instance();
            //ad.bannerEventHandler += onBannerEvent;
            //ad.interstitialEventHandler += onInterstitialEvent;
            //ad.rewardedVideoEventHandler += onRewardedVideoEvent;
            //ad.nativeBannerEventHandler += onNativeBannerEvent;
            ad.initSDK(AdMobAppId, adProperties);//reqired,adProperties can been null

            //Unity Init..
            Advertisement.Initialize(UnityAdsId);
  
        }
	}

	public static void ShowBannerAd()
	{
        //if (Application.platform == RuntimePlatform.IPhonePlayer)
        //    AdMobBinding.createBanner(AdMobBannerId, AdMobBannerType.SmartBannerLandscape, AdMobAdPosition.BottomCenter);
    }

	public static void LoadFullScreenAds()
	{
        if (Application.platform == RuntimePlatform.OSXEditor)
            return;

        if(AdCount == 0)
        {
            ad.loadInterstitial(AdMobInsterstitialID);
        }
        else if(AdCount == 1)
        {
            Chartboost.cacheInterstitial(CBLocation.Default);
        }
       
    }
	public static void ShowFullScreenAds()
	{
        if (Application.platform == RuntimePlatform.OSXEditor)
            return;

        if (AdCount == 0)
        {
            AdCount = 1;
            Advertisement.Show(); // Show Unity Ads
            ad.loadInterstitial(AdMobInsterstitialID); //Load AdMob
            // Log Flurry events.
            Flurry.EventRecordStatus status = Flurry.LogEvent("Show Unity Ads");
        }
        else if(AdCount == 1)
        {
            AdCount = 2;
            ad.showInterstitial(); //ShowAdMob
            Chartboost.cacheInterstitial(CBLocation.Default); // Load Charboost...
            // Log Flurry events.
            Flurry.EventRecordStatus status = Flurry.LogEvent("Show AdMOb Ads");
        }
        else if(AdCount == 2)
        {
            AdCount = 0;
            Chartboost.showInterstitial(CBLocation.Default);// Show Charboost...
             
            // Log Flurry events.
            Flurry.EventRecordStatus status = Flurry.LogEvent("Show Vungle Ads");
        }
    }
   
    /* Common method for ensuring logging messages have the same format */
    static void DebugLog(string message)
    {
        Debug.Log("VungleUnitySample " + System.DateTime.Today + ": " + message);
    }

}
