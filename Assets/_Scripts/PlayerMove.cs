﻿using UnityEngine;
using System.Collections;

public class PlayerMove : MonoBehaviour {

	public gameplay ObjGamePlay;
	Transform SwapTransform,SwapTransform_2;
	 Rigidbody2D SwapRigid, SwapRigid_2;

	public GameObject LeftEye, RightEye,LeftEye_Object,RightEye_Object;

    private void Awake()
    {
		//ObjGamePlay=GameObject.Find ("Script_Object").GetComponent<gameplay>();

    }

    void Start()
	{
        
		
		if (!GameObject.FindWithTag ("tagSwapTransform") && !GameObject.FindWithTag ("tagSwapTransform_2")) 
		{
			
		} 
		else 
		{
			SwapTransform_2 = GameObject.FindWithTag ("tagSwapTransform_2").transform;
			SwapTransform = GameObject.FindWithTag ("tagSwapTransform").transform;
			SwapRigid = LeftEye_Object.GetComponent<Rigidbody2D> ();
			SwapRigid_2 = RightEye_Object.GetComponent<Rigidbody2D> ();
		}

		Menu.selectedPlayer=PlayerPrefs.GetInt ("SelectedPlayers");
	}

	void Update ()
	{
		if (gameplay.isLevelClear || gameplay.isLevelFail || !gameplay.isLeft || !gameplay.isRight)
			return;
	}

	void OnCollisionEnter2D(Collision2D Other)
	{		
		 if (Other.gameObject.tag == "tagSpike") 
		{
			this.GetComponent<Rigidbody2D> ().isKinematic = true;
			iTween.ScaleTo (this.gameObject, iTween.Hash ("x",0, "y",0, "time",0.4f,"easetype",iTween.EaseType.linear));
			this.gameObject.GetComponent<CircleCollider2D> ().enabled = false;
			ObjGamePlay.auHit.Play ();
			gameplay.isLeft = false;
			gameplay.isRight = false;
			ObjGamePlay.ShowLevelFailed ();
//			Debug.Log ("GAMEPLAY_OVER-> " + gameplay.isLevelFail   + "    Left-> "+gameplay.isLeft  +"||  Right->"+gameplay.isRight   +  "     CLEARED  "+gameplay.isLevelClear);
		}

		else if (Other.gameObject.tag == "tagGround") 
		{
			ObjGamePlay.auHit.Play ();
			gameObject.GetComponent<CircleCollider2D> ().enabled = false;
//			Destroy (gameObject);
			ObjGamePlay.ShowLevelFailed ();
			Debug.Log ("Ground");
		}
		else if (Other.gameObject.tag == "tagEndPoints") 
		{
			StartCoroutine("EndGame",Other.transform);
		}

	}



	void OnTriggerEnter2D(Collider2D Other)
	{
        Debug.Log(Other.tag + " " + Other.name);
		if (Other.tag == "tagSwapTransform") 
		{
			StartCoroutine ("ScalePlayer");
			Other.GetComponent<CircleCollider2D> ().enabled = false;
		}
		else if (Other.tag == "tagSwapTransform_2") 
		{
			StartCoroutine ("ScalePlayer_2");
			Other.GetComponent<CircleCollider2D> ().enabled = false;
		}

		 else if (Other.tag == "tagCoin") 
		{
			ObjGamePlay.cntStars++;
//			Instantiate (ObjGamePlay.StarParticles,transform.position,Quaternion.identity);
			Destroy (Other.gameObject);
			ObjGamePlay.auCoin.Play ();
		}
		else if (Other.tag == "tagDimond") 
		{
			gameplay.cntDiamond++;
			Destroy (Other.gameObject);
			ObjGamePlay.auCoin.Play ();
		}
		else if (Other.tag == "tagKey") 
		{
			ObjGamePlay.auKey.Play();
			Destroy (Other.gameObject);
			gameplay.goTemp.GetComponent<CircleCollider2D> ().isTrigger = false;
			gameplay.goTemp.GetComponent<RotateObject> ().enabled = true;
			iTween.ScaleTo (gameplay.goKeyLock_Temp,iTween.Hash("x",0f,"y",0f,"time",0.01f,"easetype", iTween.EaseType.linear,"ignoretimescale",true));
		}

	}
	IEnumerator EndGame(Transform Other)
	{
		this.GetComponent<Rigidbody2D> ().isKinematic = true;
		yield return new WaitForSeconds (0.1f);
		iTween.MoveTo (this.gameObject,iTween.Hash("x",Other.position.x,"y",Other.position.y,"time",0.4f,"easetype",iTween.EaseType.easeInBounce));
		iTween.ScaleTo (this.gameObject, iTween.Hash ("x",0, "y",0, "time",0.4f,"easetype",iTween.EaseType.easeInBounce));
		this.gameObject.GetComponent<CircleCollider2D> ().enabled = false;
		ObjGamePlay.ShowLevelCleared ();
	}

	IEnumerator ScalePlayer()
	{
		this.gameObject.GetComponent<Rigidbody2D> ().isKinematic = true;
		ObjGamePlay.auIn_Swap.Play ();

		SwapTransform_2.gameObject.GetComponent<CircleCollider2D> ().enabled = false;

		LeftEye.GetComponent<Rigidbody2D> ().isKinematic = true;
		RightEye.GetComponent<Rigidbody2D> ().isKinematic = true;

		iTween.ScaleTo (this.gameObject, iTween.Hash ("x", 0, "y", 0,"time",0.2f,"easetype", iTween.EaseType.linear,"islocal", true,"ignoretimescale",true));
		yield return new WaitForSeconds (0.2f);

		Destroy(LeftEye.GetComponent<DistanceJoint2D> ());
		Destroy(RightEye.GetComponent<DistanceJoint2D>());

		yield return new WaitForSeconds (0.1f);

		GameObject.Find ("Players"+Menu.selectedPlayer+"/Right_Circle/Righteye_Object/Righteye").transform.localPosition = new Vector3 (0,0,0);
		GameObject.Find ("Players"+Menu.selectedPlayer+"/Left_Circle/Lefteye_Object/Lefteye").transform.localPosition = new Vector3 (0,0,0);

//		GameObject.Find ("Player/Righteye_Object/Righteye").transform.localPosition = new Vector3 (0,0,0);
//		GameObject.Find ("Player/Lefteye_Object/Lefteye").transform.localPosition = new Vector3 (0,0,0);


		yield return new WaitForSeconds (0.1f);

		ObjGamePlay.auOut_Swap.Play ();
		this.gameObject.transform.position =SwapTransform_2.position;

		yield return new WaitForSeconds (0.1f);
		this.gameObject.GetComponent<Rigidbody2D> ().isKinematic = false;
		iTween.ScaleTo (this.gameObject, iTween.Hash ("x",1.5, "y",1.5,"time",0.2f,"easetype", iTween.EaseType.linear,"islocal", true,"ignoretimescale",true));

		yield return new WaitForSeconds (0.1f);

		LeftEye.AddComponent<DistanceJoint2D> ();
		RightEye.AddComponent<DistanceJoint2D> ();

		LeftEye.GetComponent<DistanceJoint2D>().connectedAnchor=new Vector2 (0,0);
		RightEye.GetComponent<DistanceJoint2D>().connectedAnchor=new Vector2 (0,0);

		LeftEye.GetComponent<DistanceJoint2D> ().distance = 0.05f;
		RightEye.GetComponent<DistanceJoint2D> ().distance =0.05f;

		LeftEye.GetComponent<DistanceJoint2D> ().autoConfigureDistance = false;
		RightEye.GetComponent<DistanceJoint2D> ().autoConfigureDistance = false;

		LeftEye.GetComponent<DistanceJoint2D> ().connectedBody = SwapRigid;
		RightEye.GetComponent<DistanceJoint2D> ().connectedBody = SwapRigid_2;

		LeftEye.transform.localPosition = new Vector3 (0,0,0);
		RightEye.transform.localPosition = new Vector3 (0,0,0);

		yield return new WaitForSeconds (0.5f);

		LeftEye.GetComponent<Rigidbody2D> ().isKinematic = false;
		RightEye.GetComponent<Rigidbody2D> ().isKinematic = false;

		LeftEye.GetComponent<DistanceJoint2D> ().maxDistanceOnly = true;
		RightEye.GetComponent<DistanceJoint2D> ().maxDistanceOnly = true;

		yield return new WaitForSeconds (1.0f);

		SwapTransform.gameObject.GetComponent<CircleCollider2D> ().enabled = true;

		SwapTransform_2.gameObject.GetComponent<CircleCollider2D> ().enabled = true;
	}

	IEnumerator ScalePlayer_2()
	{
		this.gameObject.GetComponent<Rigidbody2D> ().isKinematic = true;
		ObjGamePlay.auIn_Swap.Play ();

		SwapTransform.gameObject.GetComponent<CircleCollider2D> ().enabled = false;

		LeftEye.GetComponent<Rigidbody2D> ().isKinematic = true;
		RightEye.GetComponent<Rigidbody2D> ().isKinematic = true;

		iTween.ScaleTo (this.gameObject, iTween.Hash ("x", 0, "y", 0,"time",0.2f,"easetype", iTween.EaseType.linear,"islocal", true,"ignoretimescale",true));
		yield return new WaitForSeconds (0.2f);

		Destroy(LeftEye.GetComponent<DistanceJoint2D> ());
		Destroy(RightEye.GetComponent<DistanceJoint2D>());

		yield return new WaitForSeconds (0.1f);

//		GameObject.Find ("""Player/Righteye_Object/Righteye").transform.localPosition = new Vector3 (0,0,0);
//		GameObject.Find ("Player/Lefteye_Object/Lefteye").transform.localPosition = new Vector3 (0,0,0);

		GameObject.Find ("Players"+Menu.selectedPlayer+"/Right_Circle/Righteye_Object/Righteye").transform.localPosition = new Vector3 (0,0,0);
		GameObject.Find ("Players"+Menu.selectedPlayer+"/Left_Circle/Lefteye_Object/Lefteye").transform.localPosition = new Vector3 (0,0,0);


		yield return new WaitForSeconds (0.1f);

		ObjGamePlay.auOut_Swap.Play ();
		this.gameObject.transform.position =SwapTransform.position;

		yield return new WaitForSeconds (0.1f);
		this.gameObject.GetComponent<Rigidbody2D> ().isKinematic = false;
		iTween.ScaleTo (this.gameObject, iTween.Hash ("x",1.5, "y",1.5,"time",0.2f,"easetype", iTween.EaseType.linear,"islocal", true,"ignoretimescale",true));

		yield return new WaitForSeconds (0.1f);

		LeftEye.AddComponent<DistanceJoint2D> ();
		RightEye.AddComponent<DistanceJoint2D> ();

		LeftEye.GetComponent<DistanceJoint2D>().connectedAnchor=new Vector2 (0,0);
		RightEye.GetComponent<DistanceJoint2D>().connectedAnchor=new Vector2 (0,0);

		LeftEye.GetComponent<DistanceJoint2D> ().distance = 0.05f;
		RightEye.GetComponent<DistanceJoint2D> ().distance =0.05f;

		LeftEye.GetComponent<DistanceJoint2D> ().autoConfigureDistance = false;
		RightEye.GetComponent<DistanceJoint2D> ().autoConfigureDistance = false;

		LeftEye.GetComponent<DistanceJoint2D> ().connectedBody = SwapRigid;
		RightEye.GetComponent<DistanceJoint2D> ().connectedBody = SwapRigid_2;

		LeftEye.transform.localPosition = new Vector3 (0,0,0);
		RightEye.transform.localPosition = new Vector3 (0,0,0);

		yield return new WaitForSeconds (0.5f);

		LeftEye.GetComponent<Rigidbody2D> ().isKinematic = false;
		RightEye.GetComponent<Rigidbody2D> ().isKinematic = false;

		LeftEye.GetComponent<DistanceJoint2D> ().maxDistanceOnly = true;
		RightEye.GetComponent<DistanceJoint2D> ().maxDistanceOnly = true;

		yield return new WaitForSeconds (1.0f);
	
		SwapTransform_2.gameObject.GetComponent<CircleCollider2D> ().enabled = true;
		SwapTransform.gameObject.GetComponent<CircleCollider2D> ().enabled = true;

	}
}
