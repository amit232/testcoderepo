﻿using UnityEngine;
using System.Collections;

public class btnScalling_Scripts : MonoBehaviour {

	public float toBound;
	public float animTime;
	// Use this for initialization
	void Start () 
	{
		iTween.ScaleTo (this.gameObject, iTween.Hash ("x", toBound,"y",toBound,"time", animTime, "easetype", iTween.EaseType.linear, "looptype", iTween.LoopType.pingPong, "islocal", true,"ignoretimescale",true));
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
}
