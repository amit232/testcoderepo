﻿using UnityEngine;
using System.Collections;

public class Object_Movement : MonoBehaviour {
	public Vector3 movement=Vector3.zero;
	public string moveAxis;
	public float toBound;
	public float delay;
	public float animTime;
		
	void Start()
	{
		
		if(moveAxis=="X" || moveAxis == "x") 
		{
			iTween.MoveTo (this.gameObject, iTween.Hash ("x", toBound, "time", animTime, "delay", delay, "easetype", iTween.EaseType.linear, "looptype", iTween.LoopType.pingPong, "islocal", true));
		} 
		else if (moveAxis=="Y" || moveAxis == "y") 
		{
			iTween.MoveTo (this.gameObject, iTween.Hash ("y", toBound, "time", animTime, "delay", delay, "easetype", iTween.EaseType.linear, "looptype", iTween.LoopType.pingPong, "islocal", true));
		} 
		else if (moveAxis=="Z" || moveAxis == "z") 
		{
			iTween.MoveTo(this.gameObject,iTween.Hash("z",toBound,"delay",delay,"easetype",iTween.EaseType.linear,"looptype",iTween.LoopType.pingPong,"islocal",true));
		}

		if(this.gameObject.tag == "tagSpike")
			InvokeRepeating("colliderscale",0.0f,0.1f);

	}
	void colliderscale()
	{
		if(moveAxis=="X" || moveAxis == "x") 
		{
			if (this.transform.localPosition.x > toBound || this.transform.localPosition.x < toBound) 
			{
				this.GetComponent<BoxCollider2D> ().enabled = true;
			} 
			 else if(this.transform.localPosition.x == toBound)
			{
				this.GetComponent<BoxCollider2D> ().enabled = false;
			}
		} 
		else if (moveAxis=="Y" || moveAxis == "y") 
		{
			if (this.transform.localPosition.y > toBound || this.transform.localPosition.y < toBound ) 
			{
				this.GetComponent<BoxCollider2D> ().enabled = true;
			} 
			else if(this.transform.localPosition.y == toBound)
			{
				this.GetComponent<BoxCollider2D> ().enabled = false;
			}
		}
	}
}
