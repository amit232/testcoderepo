﻿using UnityEngine;
using System.Collections;

public class DoNotDestroyOnLoad : MonoBehaviour {
	
	private static DoNotDestroyOnLoad instanceRef;
	void Awake()
	{
		if(instanceRef == null)
		{
			instanceRef = this;

			DontDestroyOnLoad(gameObject);

		} else {
			
			DestroyImmediate(gameObject);


		}
	}
	
}
